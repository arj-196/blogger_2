<?php

$servername = "localhost";
$database = "blogger_job";
$username = "root";
$password = "";


function getNewsletterEntries(){


// Create connection
    $conn = new mysqli($GLOBALS['servername'],$GLOBALS['username'],$GLOBALS['password'],$GLOBALS['database']);

    $entries = array();

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Fetching All distinct Targets
    $query = " select DISTINCT target from newsletter";

    if ($stmt = $conn->prepare($query)) {

        /* execute statement */
        $stmt->execute();

        /* bind result variables */
        $stmt->bind_result($target);

        /* fetch values */
        while ($stmt->fetch()) {
            $entries[] = [
                'target'=>json_decode($target),
                'emails'=>[],
                'offers'=>[]
            ];
        }

        /* close statement */
        $stmt->close();
    }

    // Fetching all email addresses per distinct target
    foreach($entries as $k=>$v){

        $target = json_encode($v['target']);
        $query = " select email from newsletter where target = '". $target ."'";

        if ($stmt = $conn->prepare($query)) {

            /* execute statement */
            $stmt->execute();

            /* bind result variables */
            $stmt->bind_result($email);

            $emails=[];
            /* fetch values */
            while ($stmt->fetch()) {
                $emails[]=$email;
            }
            $entries[$k]['emails']=$emails;

            /* close statement */
            $stmt->close();
        }

    }

    // Fetching latesst 10 jobs per distinct target
    foreach($entries as $k=>$v){


        $target = $v['target'];

        $query_select =
            "select offer.id, title from offer"
        ;

        $query_join =
            " join tracker"
            ." on tracker.offer_id = offer.id"
        ;

        $query_condition =
            " where privacy = 0".
            " and tracker.authorised = 1"

        ;

        $query_additional =
            " order by createdAt asc limit 10"
        ;


        if(isset($target->agency) || isset($target->city )){
            $query_join .=
                " join enterprise"
                ." on offer.enterprise_id = enterprise.id"
            ;
        }

        if(isset($target->agency)){
            $query_condition .=
                " and enterprise.name = '". $target->agency ."'"
            ;
        }

        if(isset($target->city)){

            $query_join .=
                " join address"
                ." on enterprise.address_id = address.id"
            ;

            $query_condition .=
                " and address.city = '". $target->city ."'"
            ;
        }


        $query = $query_select . $query_join . $query_condition . $query_additional;

//        print_r($target);
//        print_r($query . " \n \n \n \n ");


        if ($stmt = $conn->prepare($query)) {

            /* execute statement */
            $stmt->execute();

            /* bind result variables */
            $stmt->bind_result($id,$title);

            $offers=[];
            /* fetch values */
            while ($stmt->fetch()) {
                $offers[]=[
                    'id'=>$id,
                    'title'=>$title
                ];
            }
            $entries[$k]['offers']=$offers;

            /* close statement */
            $stmt->close();
        }

    }


    /* close connection */
    $conn->close();

    return $entries;

}

