<?php

namespace AppBundle\Twig\Extension;


use Doctrine\ORM\EntityManager;

class AppExtension extends \Twig_Extension{
    private $em;

    public function __construct(EntityManager $em){
        $this->em = $em;

    }

    public function getFilters(){
        return array(
            new \Twig_SimpleFilter('diff_time',array($this,'diffTime')),
            new \Twig_SimpleFilter('json_decode',array($this,'jsonDecode')),
            new \Twig_SimpleFilter('replace_whitespce',array($this,'replaceWhitespace')),
            new \Twig_SimpleFilter('shorten',array($this,'shortenString')),
            new \Twig_SimpleFilter('count_offer',array($this,'countOffers')),
            new \Twig_SimpleFilter('count_enterprise',array($this,'countEnterpriseType')),
            new \Twig_SimpleFilter('date_format',array($this,'dateFormat')),
        );
    }

    //TODO FIXME if more than a year different output etc
    function diffTime($date){
        $now = new \DateTime('now');
        $diff = date_diff($now,$date);
        return $diff->format("%a days ago");
    }

    public function jsonDecode($str) {
        return json_decode($str);
    }

    public function replaceWhitespace($str){
        $str = str_replace(" ","+",$str);
        $str = str_replace(".","",$str);
        $str = preg_replace('/[^A-Za-z0-9\-]/', '_', $str);
        $str = preg_replace('/_{2,}/', '_', $str);
        return $str;
    }

    public function shortenString($str, $len){
        $str = mb_convert_encoding($str,'UTF-8');
        $encode = mb_detect_encoding($str);
        if($encode=='ASCII'){
            $sub_str = substr($str, 0, $len);
        }else{
            $sub_str=$str."!TEST!";
        }
        return $sub_str;
    }

    public function countOffers(){
        return $this->em->getRepository('BJCorporateBundle:Offer')
            ->findOfferCount();
    }

    public function countEnterpriseType($type){
        return $this->em->getRepository('BJCorporateBundle:Enterprise')
            ->findEnterpriseTypeCount($type);
    }

    public function dateFormat($d){
        $months = [
            '', 'janvier', 'février',
            'mars',	'avril', 'mai',	'juin',
            'juillet', 'août', 'septembre',
            'octobre', 'novembre', 'décembre'
        ];

        $a = explode('.', $d);
        return $a[0] . ' ' . $months[intval($a[1])] . ' ' . $a[2];
    }

    public function getName(){
        return 'app_extension';
    }

}