<?php

set_time_limit(0);

require_once dirname(__DIR__) . '/../../../vendor/autoload.php';

//-- dir : /web/attachements
$dir = dirname(__DIR__) . '/../../../web/attachements/';
// list enterprises
$file_offer = fopen($dir."offers.csv","r");


try {
    //accessing database directly
    $host = "212.227.108.76";
    $dbname = "blogger_";
    $user = "blogger";
    $pass = "rAk7xnzi";

//    $host = "localhost";
//    $dbname = "blogger_job";
//    $user = "root";
//    $pass = "";

    $db = new \PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8',
        $user, $pass);

    $stmt = $db->prepare("
      select * from Offer
        join Address on Address_id = Address.id
        join Tracker on Tracker.offer_id = Offer.id
        and position_id is NULL
    ");



    $stmt->execute();
    $offers = $stmt->fetchAll(PDO::FETCH_ASSOC);

    //Settings certain details for all offer entries
    foreach($offers as $offer){
        $id = $offer['offer_id'];
        $pos = new \BJ\ElementBundle\Entity\Position();
        $address = $offer['street'] . ', ' . $offer['city']
            . ', ' . $offer['country'] . ', ' . $offer['postal'];
        $pos = get_gps_position($address);
//        var_dump($pos);
        print $address . "\n\n";

        $execute = true; //Set to true to Execute
        if($execute) {
            $stmt_1 = $db->prepare(
                " INSERT INTO Position
                (id, latitude, longitude)
                VALUES
                (:id, :lat, :lon)
            "
            );

            $lat = $pos->getLatitude();
            $lon = $pos->getLongitude();
            $stmt_1->bindParam("id", $id);
            $stmt_1->bindParam("lat", $lat);
            $stmt_1->bindParam("lon", $lon);

            if ($stmt_1->execute()) {
                $stmt_2 = $db->prepare(
                    " UPDATE Offer SET
                position_id = :id
                WHERE id = :id
            "
                );
                $stmt_2->bindParam("id", $id);
                if ($stmt_2->execute()) {
                    print "SUCCESSFUL! FOR $id \n\n";
                } else {
                    print "ERROR OFFER ! FOR $id " . $stmt_2->errorCode() . "\n\n";

                }

            } else {
                print "ERROR $id " . $stmt_1->errorCode() . " \n\n";
            }
        }
    }


}catch (PDOException $e){
    print $e->getMessage();
}


/**
 * Get Position from Address
 * @param $add
 * @return \BJ\ElementBundle\Entity\Position
 */
function get_gps_position($add){
    //Getting GPS Location
    $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($add) . '&sensor=true';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response);
    $pos = new \BJ\ElementBundle\Entity\Position();

    //var_dump($response_a);

    if (isset($response_a) && isset($response_a->results[0])
        && isset($response_a->results[0]->geometry)
        && isset($response_a->results[0]->geometry->location)
    ) {
        $pos->setLatitude($response_a->results[0]->geometry->location->lat);
        $pos->setLongitude($response_a->results[0]->geometry->location->lng);
    }
    return $pos;
}