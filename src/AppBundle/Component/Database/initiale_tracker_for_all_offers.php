<?php

set_time_limit(0);

require_once dirname(__DIR__) . '/../../../vendor/autoload.php';

//-- dir : /web/attachements
$dir = dirname(__DIR__) . '/../../../web/attachements/';
// list enterprises
$file_offer = fopen($dir."offers.csv","r");


try {
    //accessing database directly
    $host = "212.227.108.76";
    $dbname = "blogger_";
    $user = "blogger";
    $pass = "rAk7xnzi";

//    $host = "localhost";
//    $dbname = "blogger_job";
//    $user = "root";
//    $pass = "";

    $db = new \PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8',
        $user, $pass);

    $stmt = $db->prepare("
        select * from Offer
    ");

    $stmt->execute();
    $offers = $stmt->fetchAll(PDO::FETCH_ASSOC);

    //Settings certain details for all offer entries
    foreach($offers as $offer){
        $id = $offer['id'];

        //Creating tracker entries for all offers
        $stmt_offer_1 = $db->prepare(
            " insert into Tracker
                (offer_id, viewed, authorised, expired, password)
              VALUES
                (:id, true, true, 0, '')
            "
        );
        $stmt_offer_1->bindParam('id', $id);
        $status = $stmt_offer_1->execute();
        if(!$status){
            print $stmt_offer_1->errorCode();
            print "ERROR TRACKER FOR ID : ". $id . " \n\n";
        }
    }


}catch (PDOException $e){
    print $e->getMessage();
}
