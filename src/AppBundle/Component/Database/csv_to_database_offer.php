<?php

set_time_limit(0);

require_once dirname(__DIR__) . '/../../../vendor/autoload.php';

//-- dir : /web/attachements
$dir = dirname(__DIR__) . '/../../../web/attachements/';
// list enterprises
$file_offer = fopen($dir."offers.csv","r");

//Variables
$contract_type = [
    '2868' =>'STAGE',
    '1932' =>'CDI',
    '1933' =>'CDD',
    '2866' =>'ALTERNANCE',
    '2867' =>'FREELANCE'
];

try {
    //accessing database directly
    $host = "212.227.108.76";
    $dbname = "blogger_";
    $user = "blogger";
    $pass = "rAk7xnzi";

//    $host = "localhost";
//    $dbname = "blogger_job";
//    $user = "root";
//    $pass = "";

    $db = new \PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8',
        $user, $pass);

    //fetching username
    $stmt = $db->prepare(
        "INSERT INTO Offer
            ( id,
            enterprise_id,
              title, type, post, description, mission, profile,
              asap, dateStart, duration, salary, createdAt, privacy, tags )
              values
              ( :offer_id, :enterprise_id,
                :title, :type, :post, :description, :mission, :profile,
                 :asap, :dateStart, :duration, :salary, :createdAt, :privacy, :tags )"
    );
    $stmt->bindParam(':offer_id', $offer_id);
    $stmt->bindParam(':enterprise_id', $enterprise_id);
    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':type', $type);
    $stmt->bindParam(':post', $post);
    $stmt->bindParam(':description', $description);
    $stmt->bindParam(':mission', $mission);
    $stmt->bindParam(':profile', $profile);
    $stmt->bindParam(':asap', $asap);
    $stmt->bindParam(':dateStart', $dateStart);
    $stmt->bindParam(':duration', $duration);
    $stmt->bindParam(':salary', $salary);
    $stmt->bindParam(':createdAt', $createdAt);
    $stmt->bindParam(':privacy', $privacy);
    $stmt->bindParam(':tags', $tags);

    $stmt_email = $db->prepare(
        "INSERT INTO Email
            (id, email, comment)
            values
            (:email_id, :email_address, '')
            "
    );
    $stmt_email->bindParam(':email_id', $email_id);
    $stmt_email->bindParam(':email_address', $email_address);

    $stmt_address = $db->prepare(
        "INSERT INTO Address
          (id, street, postal, city, country)
          VALUES
          (:address_id, :address_street, :address_postal, :address_city, :address_country)"
    );
    $stmt_address->bindParam(':address_id', $address_id);
    $stmt_address->bindParam(':address_street', $address_street);
    $stmt_address->bindParam(':address_postal', $address_postal);
    $stmt_address->bindParam(':address_city', $address_city);
    $stmt_address->bindParam(':address_country', $address_country);

    $stmt_phone = $db->prepare(
        "INSERT INTO Phone
          (id, number, comment)
          VALUES
          (:phone_id, null, '')
            "
    );
    $stmt_phone->bindParam(':phone_id', $phone_id);

    $i=0;
    $list_offers = new \Doctrine\Common\Collections\ArrayCollection();
    if (($handle = $file_offer) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
            $len = count($data);
            if($len == 17 && 0!=$i++) //verify data format is right
            {
                //Offer
                $offer_id = $i;
                $createdAt = mb_convert_encoding($data[0],'UTF-8');
                $type = $contract_type[mb_convert_encoding($data[1],'UTF-8')];
                $post = mb_convert_encoding($data[2],'UTF-8');
                $title= mb_convert_encoding($data[3],'UTF-8');
                $description = mb_convert_encoding($data[7],'UTF-8');
                $mission = mb_convert_encoding($data[8],'UTF-8');
                $profile = mb_convert_encoding($data[9],'UTF-8');
                $duration = mb_convert_encoding($data[10],'UTF-8');
                $salary = mb_convert_encoding($data[13],'UTF-8');
                if(mb_convert_encoding($data[15],'UTF-8')=='now')
                    $asap=1;
                else
                    $asap=0;

                $dateStart = mb_convert_encoding($data[16],'UTF-8');
                $tags = mb_convert_encoding($data[5],'UTF-8');

                //Phone
                $phone_id = $i;

                //Email
                $email_address = mb_convert_encoding($data[12],'UTF-8');
                $email_id = $i;

                //Address + Position
                $address_full = mb_convert_encoding($data[11],'UTF-8');
                $address = new \BJ\ElementBundle\Entity\Address();
                $address_id = $i;

                $add1 = explode(',',$address_full);
                if(!empty($add1)){
                    if( !isset($add1[0]) || !isset($add1[1]))
                        continue;

                    $address_street = $add1[0]; $address->setStreet($add1[0]);
                    $add2 = explode(' ',trim($add1[1]));
                    $address_postal = $add2[0]; $address->setPostal($add2[0]);
                    $address_city = $add2[1]; $address->setCity($add2[1]);
                    $address_country = 'France'; $address->setCountry('France');
                }

                //enterprise
                $enterprise_name = mb_convert_encoding($data[6],'UTF-8');
                $stmt_e = $db->prepare(" select * from Enterprise where Enterprise.name = '".$enterprise_name."' ");
                $stmt_e->execute();
                $enterprise = $stmt_e->fetchAll(PDO::FETCH_ASSOC);
                if(isset($enterprise[0]['id'])){
                    $enterprise_id = $enterprise[0]['id'];
                }
                else{
                    $enterprise_id = null;
                }

                //Privacy settings
                if(mb_convert_encoding($data[4],'UTF-8')=='publish')
                    $privacy = 0;
                else
                    $privacy = 1;

                /*
                 * Storing offer in database
                 */
                if(!$list_offers->contains(trim(strtolower($title))) && $enterprise_id != null)
                {
                    //Executing the statement
                    $status = $stmt->execute();
                    if($status){
                        $stmt_address->execute();
                        $stmt_phone->execute();
                        $stmt_email->execute();
                        $list_offers->add(trim(strtolower($title)));
                    }else{
                        print_r("ERROR INSERTING : ");
                        print_r($stmt->errorCode());
                    }

                    //Printing results
                    if($status){
                        var_dump("STORING TO DATABASE");
                        print_r("\n--------------------------------\n");
                        var_dump("ENTERPRISE_NAME");
                        var_dump($enterprise);
                        print_r("\n < '". $createdAt . "'");
                        print_r("\n < '". $type . "'");
                        print_r("\n < '". $post . "'");
                        print_r("\n < '". $title . "'");
                        print_r("\n < '". $description . "'");
                        print_r("\n < '". $mission . "'");
                        print_r("\n < '". $profile . "'");
                        print_r("\n < '". $duration . "'");
                        print_r("\n < '". $salary . "'");
                        print_r("\n < '". $asap . "'");
                        print_r("\n < '". $dateStart . "'");
                        print_r("\n < '". $tags . "'");
                        print_r("\n < '". $phone_id . "'");
                        print_r("\n < '". $address_id . "'");
                        print_r("\n < '". $email_id . "'");
                        print_r("\n < '". $email_address . "'");
                        print_r("\n < '". $address_full . "'");
                        print_r("\n < '". $address_street . "'");
                        var_dump($address);
                        print_r("\n < '". $enterprise_name . "'");
                        print_r("\n < '". $privacy . "'");

                        print_r("\n--------------------------------\n");
                    }else{
                        print_r("ERROR IN INSERTING SO NO PRINT \n");
                    }
                }
            }

        }
        fclose($handle);
    }
}
catch (PDOException $e){
    print $e->getMessage();
}






