<?php

set_time_limit(0);

require_once dirname(__DIR__) . '/../../../vendor/autoload.php';

//-- dir : /web/attachements
$dir = dirname(__DIR__) . '/../../../web/attachements/';
// list enterprises
$file_offer = fopen($dir."offers.csv","r");

//Variables
$contract_type = [
    '2868' =>'STAGE',
    '1932' =>'CDI',
    '1933' =>'CDD',
    '2866' =>'ALTERNANCE',
    '2867' =>'FREELANCE'
];

try {
    //accessing database directly
    $host = "212.227.108.76";
    $dbname = "blogger_";
    $user = "blogger";
    $pass = "rAk7xnzi";

//    $host = "localhost";
//    $dbname = "blogger_job";
//    $user = "root";
//    $pass = "";

    $db = new \PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8',
        $user, $pass);

    $i=0;
    $list_offers = new \Doctrine\Common\Collections\ArrayCollection();
    if (($handle = $file_offer) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
            $len = count($data);
            if($len == 17 && 0<$i++) //verify data format is right
            {
                //Offer
                $title= mb_convert_encoding($data[3],'UTF-8');

                //Phone
                $phone_id = $i;

                //Email
                $email_address = mb_convert_encoding($data[12],'UTF-8');
                $email_id = findMatch($email_address, 'Email', 'email', $db, $i);

                //Address
                $address_full = mb_convert_encoding($data[11],'UTF-8');
                $address = new \BJ\ElementBundle\Entity\Address();

                $add1 = explode(',',$address_full);
                if(!empty($add1)){
                    $address_street = $add1[0]; $address->setStreet($add1[0]);
                    $add2 = explode(' ',trim($add1[1]));
                    $address_postal = $add2[0]; $address->setPostal($add2[0]);
                    $address_city = $add2[1]; $address->setCity($add2[1]);
                    $address_country = 'France'; $address->setCountry('France');
                }
                $address_id = findMatch($address->getStreet(), 'Address', 'street', $db, $i);

                /*
                 * Storing offer in database
                 */
                if(!$list_offers->contains(trim(strtolower($title))))
                {
                    //Executing the statement
                    if($status = ( !is_null($address_id) && !is_null($email_id) )){
                        //Find offer
                        $stmt_offer = $db->prepare(
                            " select * from Offer
                                where title = :title "
                        );
                        $stmt_offer->bindParam("title",$title);
                        $stmt_offer->execute();
                        $res = $stmt_offer->fetchAll(PDO::FETCH_ASSOC);
                        if ( $status = (count($res)==1)){
                            $offer_id = $res[0]['id'];

                            $stmt_update = $db->prepare(
                                "update Offer
                                    set address_id = :address_id, email_id = :email_id
                                    where id = :id
                                    "
                            );
                            $stmt_update->bindParam("id", $offer_id);
                            $stmt_update->bindParam("address_id", $address_id);
                            $stmt_update->bindParam("email_id", $email_id);

                            if(!$stmt_update->execute()){
                                print " \n\nERROR BIGTIME at $i \n";
                                print " Counld not update \n";
                                print  $stmt_update->errorCode() . " \n";
                                print "---------------------------\n";
                            }

                            //Printing results
                            if($status){
                                print_r("\n < title '". $title . "'");
                                print_r("\n < phone id '". $phone_id . "'");
                                print_r("\n < address id '". $address_id . "'");
                                print_r("\n < email id '". $email_id . "'");
                                print_r("\n < email address '". $email_address . "'");
                                print_r("\n < address full '". $address_full . "'");
                                var_dump($address);

                                print_r("\n--------------------------------\n");
                            }
                        }else{
                            print " \n\nERROR BIGTIME at $i \n";
                            print " found no or more than one offer \n";
                            print "---------------------------\n";
                        }
                    }else{

                        print " \n\nERROR BIGTIME at $i \n";
                        print " one of id is null \n";
                        print "---------------------------\n";
                    }
                }
            }

        }
        fclose($handle);
    }
}
catch (PDOException $e){
    print $e->getMessage();
}

function findMatch($target, $mode, $mode_target, $db, $i){
    $id = null;
    $stmt = $db->prepare(
        " select * from $mode
                      where $mode_target = :$mode "
    );
    $stmt->bindParam($mode, $target);
    $stmt->execute();
    $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if(!empty($res)){
        //Checking if entry already exits or not to prevent unique error
        foreach($res as $r){
            $id = $r['id'];
            $stmt = $db->prepare(
                " select * from offer
                      where ".$mode.". id = :id "
            );
            $stmt->bindParam('id', $id);
            $stmt->execute();
            $occ = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(empty($occ)){
                print "FOUND ". strtoupper($mode). ": $id \n";
                break;
            }
        }
    }
    else{
        print $stmt->errorCode() . "\n";
        print "ERROR FOR ID : ". $i . " \n\n";
    }
    return $id;
}






