<?php

namespace AppBundle\Component\Adminstation;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Service in charge of handling Client Admin Actions
 * Class ClientAdmin
 * @package AppBundle\Component\Adminstation
 */
class ClientAdmin {
    private $em;
    private $fs;
    private $root;

    /**
     * Constructor
     * @param EntityManager $em
     * @param Filesystem $fs
     * @param $root
     * @param $cachePath
     */
    public function __construct( EntityManager $em, Filesystem $fs, $root ){
        $this->em = $em;
        $this->fs = $fs;
        $this->root = $root;
        $this->verifyDirs();
    }

    /**
     * Verifying Directory Structure
     */
    public function verifyDirs(){
        if(!is_dir($this->root)){
            $this->fs->mkdir($this->root);
        }
    }



}