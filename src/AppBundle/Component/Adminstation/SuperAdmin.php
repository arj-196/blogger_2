<?php

namespace AppBundle\Component\Adminstation;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Service in charge of handling Super Admin Actions
 * Class SuperAdmin
 * @package AppBundle\Component\Adminstation
 */
class SuperAdmin {
    private $em;
    private $fs;
    private $root;
    private $cache;
    private $previous;
    private $rep;

    /**
     * Constructor
     * @param EntityManager $em
     * @param Filesystem $fs
     * @param $root
     * @param $cachePath
     */
    public function __construct( EntityManager $em, Filesystem $fs, $root , $cachePath ){
        $this->em = $em;
        $this->fs = $fs;
        $this->root = $root;
        $this->cache = $cachePath;
        $this->verifyDirs();
        $this->rep = $this->em ->getRepository('BJCorporateBundle:Tracker');
        $content = file_get_contents($this->cache);
        $this->previous = json_decode($content);

        //Test
//        var_dump($this->previous);
    }

    /**
     * Verifying Directory Structure
     */
    public function verifyDirs(){
        if(!is_dir($this->root)){
            $this->fs->mkdir($this->root);
        }
    }

    /**
     * Clearing Cache
     */
    public function createCache(){
        //Retrieving previous cache
        $content = file_get_contents($this->cache);
        $this->previous = json_decode($content);

        //Creating new cache
        $file = fopen($this->cache,'w');
        $arr = [];

        //Last Accessed
        $arr['last_accessed'] = date_format(new \DateTime('now'),'d-m-Y');

        $jsonContent = json_encode($arr);
        fwrite($file,$jsonContent);
        fclose($file);
    }

    /**
     * Getting Global Information for Super Admin Dashboard
     * @return array
     */
    public function getGlobalInfo(){
        $arr=[];

        $arr['total_offers'] = $this->rep
            ->findCountOffer()
        ;
        $arr['total_offers_authorised'] = $this->rep
            ->findCountOfferAuthorised()
        ;
        $arr['total_offers_un_authorised'] = $this->rep
            ->findCountOfferUnAuthorised()
        ;

        $arr['total_offers_new'] = $this->rep
            ->findCountOfferNew(
                $this->previous->last_accessed
            )
        ;

        $arr['total_offers_new_authorised'] = $this->rep
            ->findCountOfferNewAuthorised(
                $this->previous->last_accessed
            )
        ;

        $arr['total_offers_new_un_authorised'] = $this->rep
            ->findCountOfferNewUnAuthorised(
                $this->previous->last_accessed
            )
        ;


        return $arr;
    }

    /**
     * Getting offers for an action
     * @param $action
     * @param $iteration
     * @param $max
     * @return array
     */
    public function getOffers($action, $iteration, $max){
        $this->rep = $this->em->getRepository('BJCorporateBundle:Tracker');
        $offers=[];

        if($action=='offer_auth'){
            $offers = $this->rep->findOffersAuthorised($iteration, $max);

        }else if($action=='offer_un_auth'){
            $offers = $this->rep->findOffersUnAuthorised($iteration, $max);

        }else if($action=='offer_new_auth'){
            $offers = $this->rep->findOffersNewAuthorised(
                $this->previous->last_accessed,
                $iteration,
                $max
            );

        }else if($action=='offer_new_un_auth'){
            $offers = $this->rep->findOffersNewUnAuthorised(
                $this->previous->last_accessed,
                $iteration,
                $max
            );
        }
        return $offers;
    }

    /**
     * Removing Outdated Offers
     */
    public function removeOutdatedOffers(){
        //TODO FIXME finish
        $offers = $this->em->getRepository('BJCorporateBundle:Tracker')
            ->findOutdatedOffers();

        var_dump(count($offers));
    }

    /**
     * Get all newsletter sorted by distinct target
     * @return array
     */
    public function getNewsletters(){

        $targets = $this->em->createQuery(
            ' SELECT distinct n.target'.
            ' FROM BJCorporateBundle:Newsletter n'
            )
            ->getResult()
        ;

        $results=[];
        foreach($targets as $v){
            $res['target'] = $v['target'];
            $res['emails'] = $this->em->createQuery(
                 ' SELECT n.email'.
                 ' FROM BJCorporateBundle:Newsletter n'.
                 ' WHERE n.target = :target'
                )
                ->setParameter('target',$v['target'])
                ->getResult()
            ;
            $results[] = $res;
        }

        return $results;
    }

}