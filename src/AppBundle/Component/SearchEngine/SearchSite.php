<?php

namespace AppBundle\Component\SearchEngine;

use Doctrine\ORM\EntityManager;

class SearchSite
{
    private $em;

    public function __construct(EntityManager $em){
        $this->em = $em;
    }

    public function doSearch($query){
        return $this->em->getRepository('BJCorporateBundle:Offer')
            ->findOffersByPattern($query);
    }

    public function getSuggestions($query){

        return $this->em->getRepository('BJCorporateBundle:Offer')
            ->findOffersByPattern($query);
    }

    public function getEnterpriseSuggestions($query){
        return $this->em->getRepository('BJCorporateBundle:Enterprise')
            ->findOffersByPattern($query);
    }

    public function getEnterpriseByName($name){
        $enterprises = $this->em->getRepository('BJCorporateBundle:Enterprise')
            ->findBy(array('name' => $name));

        if(sizeof($enterprises) > 0){
            $enterprise = $enterprises[0];
            $offers = $enterprise->getOffers();

            if(sizeof($offers) > 0){
                $offer = $offers[sizeof($offers) - 1];
                $address = $offer->getAddress();
                if(!is_null($address)){
                    $address = array(
                        'street'=> $address->getStreet(),
                        'postal'=> $address->getPostal(),
                        'city'=> $address->getCity(),
                        'country'=> $address->getCountry(),

                    );
                }

                $email = $offer->getEmail();
                if(!is_null($email)){
                    $email = array(
                        'email' => $email->getEmail()
                    );
                }

                $phone = $offer->getPhone();
                if(!is_null($phone)){
                    $phone = array(
                        'phone' => $phone->getNumber()
                    );
                }

            } else {
                $address = null;
                $phone = null;
                $email = null;
            }

            $data = array(
                'id'=> $enterprise->getId(),
                'name'=> $enterprise->getName(),
                'description'=> $enterprise->getDescription(),
                'a'=> $address,
                'e'=> $email,
                'p'=> $phone,
            );
        } else {
            $data = null;
        }

//        var_dump($offer);

        return $data;
    }

}