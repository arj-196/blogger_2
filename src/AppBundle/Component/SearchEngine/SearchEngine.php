<?php

namespace AppBundle\Component\SearchEngine;

use AppBundle\Component\ErrorLog;
use AppBundle\Component\YamlFileToArrayLoader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;


class SearchEngine {
    private $em;
    private $root;
    private $tagFile;
    private $results;
    private $repOffer;
    private $log;

    /**
     * Constructor
     * @param $tagFile
     * @param EntityManager $em
     * @param ErrorLog $log
     */
    public function __construct( $tagFile, EntityManager $em, ErrorLog $log ){
        $this->root = __DIR__.'/';
        $this->tagFile = __DIR__.'/'.$tagFile;
        $this->em = $em;
        $this->log = $log;
        $this->results = null;
    }

    /**
     * DEPRECIATED
     * Constructing Filter Option Lists
     * @return array
     */
    public function filterOptions(){
        $option=[];

        $option['contracts'] = array(
            array('type' => 'ALTERNANCE'),
            array('type' => 'CDD'),
            array('type' => 'CDI'),
            array('type' => 'FREELANCE'),
            array('type' => 'STAGE'),
        )
        ;

        $option['posts'] = array(
            array('post' => 'commercial', 'value' => 'commercial'),
            array('post' => 'création', 'value' => 'creation'),
            array('post' => 'planning stratégique', 'value' => 'planning_strategique'),
            array('post' => 'relations publiques / event', 'value' => 'relations publique / event'),
            array('post' => 'social media', 'value' => 'social media'),
            array('post' => 'technique (html, ux...)', 'value' => 'technique'),
        )
        ;

        $option['chez'] = array(
            array('type' => 'agence'),
            array('type' => 'annonceur'),
        );

        $option['enterprises'] = $this->em
            ->createQuery(
                ' SELECT DISTINCT e.name'.
                ' FROM BJCorporateBundle:Enterprise e'.
                ' ORDER BY e.name ASC'
            )
            ->getResult()
        ;

        return $option;
    }


    /**
     * DEPRECIATED
     * Constructing Filter Option Lists
     * @return array
     */
    public function filterOptions2(){
        $option=[];

        $option['contracts'] = $this->em
            ->getRepository('BJCorporateBundle:Offer')
            ->findDistinctCategories()
        ;

        $option['posts'] = $this->em
            ->createQuery(
                ' SELECT DISTINCT o.post'.
                ' FROM BJCorporateBundle:Offer o'.
                ' ORDER BY o.post ASC'
            )
            ->getResult()
        ;

        $option['chez'] = $this->em
            ->createQuery(
                ' SELECT DISTINCT e.type'.
                ' FROM BJCorporateBundle:Enterprise e'.
                ' ORDER BY e.type ASC'
            )
            ->getResult()
        ;

        $option['enterprises'] = $this->em
            ->createQuery(
                ' SELECT DISTINCT e.name'.
                ' FROM BJCorporateBundle:Enterprise e'.
                ' ORDER BY e.name ASC'
            )
            ->getResult()
        ;

        return $option;
    }

    /**
     * Depreciated
     * Init Function
     * Looks for relevant tags and Soundex Matching
     * Fetches offers from database
     * @param $options
     */
    public function init( $options ){
        //TODO FIXME handle validity
        $this->repOffer = $this->em
            ->getRepository('BJCorporateBundle:Offer');
        //var_dump('Initial',$options);
        $tags = new ArrayCollection();
        //Loading tag Data
        $ymlLoader = new YamlFileToArrayLoader(new FileLocator($this->root));
        $data = $ymlLoader->load($this->tagFile);

        //Soundex matching
        foreach($data['tags'] as $kd=>$vd){
            foreach($options['tags'][0] as $kt=>$vt){
                if( soundex($vd) == soundex($vt)){
                    $tags[] = $vd;
                }
            }
        }

        //Regex matching
        foreach($tags as $kt=>$vt){
            foreach($data['tags'] as $kd=>$vd){
                $reg="/".$vt."/";
                if(preg_match($reg,$vd)){
                    if(!$tags->contains($vd)){
                        $tags[] = $vd;
                    }
                }
            }
        }

        $offers= [];
        $count = 0;
        //TODO if no tags found ? what to do ?
        if(!$tags->isEmpty()){
            if(empty($options['location'])){
                $offers = $this->repOffer->findOffersByTags($tags);
                $count = $this->repOffer->findOfferCountByTags($tags);
            }
        }


        $this->results = [
            'offers'=>$offers,
            'count'=>$count[0][1]
        ];
    }

    /**
     * Get Results
     * @return $results
     */
    public function getResults(){
        return $this->results;
    }

}
