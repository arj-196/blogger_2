<?php

namespace AppBundle\Component\Email;


use Mailgun\Mailgun;

class MailgunMailer
{
    private $mgClient;
    private $domain;
    private $from;

    public function __construct(){
        # Instantiate the client.
        $this->mgClient = new Mailgun('key-0564f638a373dc2e6a7ca59cf2b1a629');
        $this->domain = "sandbox0e343d1fdf074f37a630edd44355b219.mailgun.org";
        $this->from = 'unjobdanslapub <postmaster@unjobdanslapub.fr>';
    }

    public function send_mail($to, $subject, $body, $attachement){
        $m = $this->mgClient->MessageBuilder();
        $m->setFromAddress($this->from);
        $m->addToRecipient($to);
        $m->setSubject($subject);
        $m->setHtmlBody($body);
        if(!is_null($attachement)){
            $m->addAttachment($attachement);
        }


        $result = $this->mgClient->sendMessage("$this->domain", $m->getMessage(), $m->getFiles());

    }

}