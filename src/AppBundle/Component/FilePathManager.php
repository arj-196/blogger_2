<?php
namespace AppBundle\Component;


use Symfony\Component\Config\FileLocator;

/**
 * List of all required dir and file paths
 * Class FilePathManager
 * @package AppBundle\Component
 */
class FilePathManager {
    private $paths;

    /**
     * constructor
     * @param ErrorLog $logger
     */
    public function __construct(){
        $this->paths = [
            'root_dir' => 'tmp',
            'header_autocomplete' => 'tmp/header_autocomplete.json',
            'search_engine_root' => 'tmp/search',
            'tag_data' => '../data.yml',
            'super_admin_cache' => 'tmp/super_admin_cache.json',
        ];
    }

    /**
     * get root directory
     * @return string
     */
    public function getRootDir(){
        return $this->paths['root_dir'];
    }

    /**
     * get json file for autocompleter in header
     * @return string
     */
    public function getPathHeaderAutocomplete(){
        return $this->paths['header_autocomplete'];
    }


    /**
     * get directory for json search files
     * @return mixed
     */
    public function getSearchEngineRootDir(){
        return $this->paths['search_engine_root'];
    }

    /**
     * @return mixed
     */
    public function getTagDataFile(){
        return $this->paths['tag_data'];
    }

    public function getSuperAdminCache(){
        return $this->paths['super_admin_cache'];
    }
}