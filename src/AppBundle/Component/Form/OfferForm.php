<?php

namespace AppBundle\Component\Form;

use BJ\CorporateBundle\Entity\Enterprise;
use BJ\CorporateBundle\Entity\Offer;
use BJ\CorporateBundle\Entity\Tracker;
use BJ\ElementBundle\Entity\Address;
use BJ\ElementBundle\Entity\Email;
use BJ\ElementBundle\Entity\Phone;
use BJ\ElementBundle\Entity\Position;
use Doctrine\ORM\EntityManager;

class OfferForm
{
    private $em;

    public function __construct(EntityManager $em){
        $this->em = $em;
    }

    public function handle($mode, $data){
        if($mode == 'NEW_OFFER'){
            return $this->createNewOffer($data);
        }

        return null;
    }

    private function createNewOffer($data){

        $offer = $this->createOffer($data);

        $enterprise_id = $data->get('f-opt-ent');
        $enterprise = $this->em->getRepository('BJCorporateBundle:Enterprise')
            ->find($enterprise_id);

        if(!is_null($enterprise)){
            $offer->setEnterprise($enterprise);
            $enterprise->setDescription($data->get('f-o-d'));
        } else {
            $enterprise = new Enterprise();
            $enterprise->setName($data->get('fr-enterprise'));
            $enterprise->setType($data->get('fr-secteur-1'));
            $enterprise->setDescription($data->get('f-o-d'));
            $offer->setEnterprise($enterprise);
        }

        $this->em->persist($enterprise);
        $this->em->persist($offer);
        $this->em->persist($offer->getTracker());
        $this->em->flush();
        $this->em->refresh($offer);

//        var_dump($data);
//        var_dump($offer);
//        var_dump($enterprise);

        return     $settings = [
            'action' => 'posted',
            'enterprise' => $enterprise,
            'offer' => $offer
        ];
    }

    private function createoffer($data){
        $offer = new Offer();
        // type
        $offer->setType($data->get('f-c-t'));
        // post
        $offer->setPost($this->getPost($data));
        // title
        $offer->setTitle($this->getTitle($data));
        // description
        // TODO offer doesn't have description, enterprise does
        $offer->setDescription($data->get('f-o-d'));
        // mission
        $offer->setMission($data->get('f-o-d-m'));
        // profile
        $offer->setProfile($data->get('f-o-d-p'));
        // duration
        $offer->setDuration($this->getDuration($data));
        // salary
        $offer->setSalary($this->getSalary($data));
        // date start
        $offer->setDateStart($this->getDateStart($data));
        // asap
        $asap = $data->get('f-d-a');
        if (isset($asap)) {
            $offer->setAsap(true);
        } else {
            $offer->setAsap(false);
        }
        // privacy
        $offer->setPrivacy(intval($data->get('privacy')));
        // tracker
        $offer->setTracker($this->createTracker($offer));
        // email
        $offer->setEmail(new Email(trim($data->get('f-e'))));
        // phone
        $offer->setPhone(new Phone(trim($data->get('f-p'))));

        // address and position
        $addNPos = $this->createAddressAndPosition($data);
        $offer->setAddress($addNPos['address']);
        $offer->setPosition($addNPos['position']);
        return $offer;
    }

    private function getTitle($data){
        return $data->get('f-c-t') . ' : ' . $data->get('fr-post') . ' chez ' . $data->get('fr-enterprise');
    }

    private function getPost($data){
        $p2 = $data->get('f-pt-2');
        if(isset($p2)){
            return $p2;
        } else {
            return $data->get('f-pt-1');
        }
    }

    private function getDuration($data){
        $duration_autre = $data->get('fr-duree-1');
        if(!isset($duration_autre)){
            return $data->get('f-du-2');
        } else {
            return $data->get('f-du-3');
        }
    }

    private function getSalary($data){
        return $data->get('f-c-s-1') . ' par ' . $data->get('fr-salary-per');
    }

    private function getDateStart($data){
        if($data->get('f-d-a') == 'date'){
            $ds = explode('/', $data->get('f-d-s'));
            return new \DateTime($ds[2] . "-" . $ds[1] . '-' . $ds[0]);
        } else {
            return new \DateTime("now");
        }
    }

    private function createTracker($offer){
        $tracker = new Tracker();
        $tracker->setAuthorised(false);
        $tracker->setViewed(false);
        $tracker->setOffer($offer);
        return $tracker;
    }

    private function createAddressAndPosition($data){
        $address = new Address(trim($data->get('f-l-1')), trim($data->get('f-l-2')), trim($data->get('f-l-3')), trim($data->get('f-l-4')));
        $add = $address->getStreet() . ', ' . $address->getCity()
            . ', ' . $address->getCountry() . ', ' . $address->getPostal();

        //Getting GPS Location
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($add) . '&sensor=true';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        $pos = new Position();

        if (isset($response_a) && isset($response_a->results[0])
            && isset($response_a->results[0]->geometry)
            && isset($response_a->results[0]->geometry->location)
        ) {
            $pos->setLatitude($response_a->results[0]->geometry->location->lat);
            $pos->setLongitude($response_a->results[0]->geometry->location->lng);
        }
        return array('address' => $address, 'position'=> $pos);
    }
}