<?php

namespace AppBundle\Component;


use Symfony\Component\Filesystem\Filesystem;

/**
 * Custom Error Logger
 * Class ErrorLog
 * @package AppBundle\Component
 */
class ErrorLog {
    private $fs;
    private $root = 'tmp';
    private $filePath = 'tmp/log.txt';

    public function __construct(Filesystem $fs){
        $this->fs = $fs;
    }

    public function quick($log){
        $this->entry('',$log);
    }

    /**
     * Printing Log
     * @param $purpose
     * @param $log
     */
    public function entry($purpose, $log){
        if(!is_dir($this->root)){
            $this->fs->mkdir($this->root);
        }
        if($file = fopen($this->filePath,'a')) {
            fwrite($file, "------------------------------------------------------------"."\n");
            //Print Time
            $t=time();
            ob_start();
            print_r(date("Y-m-d h:i:sa",$t));

            //Print Purpose
            $result = ob_get_clean();
            fwrite($file, $result."\n");
            fwrite($file, "PURPOSE: ".$purpose."\n");

            //Print Log
            fwrite($file, "----"."\n");
            ob_start();
            print_r($log);
            $result = ob_get_clean();
            fwrite($file, $result."\n");
            fwrite($file, "----"."\n");

            //Tracing Parent Caller
            $trace = debug_backtrace();
            if (isset($trace[1])) {
                fwrite($file, "Called by: ");
                fwrite($file, $trace[1]['class']."\n");
            }

            fclose($file);
        }
    }
}