function l(str){
    console.log(str);
}

/**
 *  DEPRECIATED
 *  Setting up Post Dynamic Features
 *  Enterprise List
 */
function setupPost(list)
{
    for(j in list){
        var name = list[j]['name'];
        var enterprises = config[name];
        var container = $(list[j]['data']['container']);

        for(i in enterprises){
            var li = $(document.createElement('li'));
            var text = $(document.createElement('span'));
            var close = $(document.createElement('i'));

            text.text(enterprises[i]['name']);
            close.attr('class', 'fa fa-times pull-right ent-close');
            close.bind('click', entOnClose);
            text.bind('click', entOnClick);

            li.attr('dt-type', name);
            li.append(text);
            li.append(close);
            container.append(li);
        }

    }

    l('setup done - finish');
}

/**
 * DEPRECIATED
 * Enterprise entry on click
 */
function entOnClick()
{
    var type = $(this).parent().attr('dt-type');
    var container;
    var target;
    for(j in ent_ref){
        if(ent_ref[j]['name'] == type){
            container = $(ent_ref[j]['data']['container']);
            target = $('#'+ent_ref[j]['data']['target']);
        }
    }
    container.find('i').css('visibility', 'hidden');
    var text = $(this).text();
    $(this).siblings('i').css('visibility' ,'visible');
    target.val(text);
    target.prop('readonly', true);
}

/**
 * DEPRECIATED
 * Enterprise close on click
 */
function entOnClose()
{
    var type = $(this).parent().attr('dt-type');
    var container;
    var target;
    for(j in ent_ref){
        if(ent_ref[j]['name'] == type){
            container = $(ent_ref[j]['data']['container']);
            target = $('#'+ent_ref[j]['data']['target']);
        }
    }

    container.find('i').css('visibility', 'hidden');
    target.val('');
    target.prop('readonly', false);

}