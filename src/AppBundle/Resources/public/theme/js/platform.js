function Platform(){

    this.init = function(){
        var navBar = new NavBar();
        navBar.setCurrentUrl();
    };


    // main --
    this.init();
}

function NavBar(){
    this.setCurrentUrl = function(){
        var currUrl = getCurrentUrl();
        l(currUrl);
        $.each($('#navbar-main').find('a'), function(){
            var href = $(this).attr('href');
            if(href[href.length - 1] == '/'){
                href = href.slice(0, href.length - 1);
            }
            if(currUrl.toLowerCase().trim() == href.toLowerCase().trim()){
                $(this).addClass('btn-blue-2');
            }
        });
    };

    var getCurrentUrl = function(){
        var u = window.location.href;
        if(u[u.length - 1] == '#'){
            u = u.slice(0, u.length - 1);
        }
        if(u[u.length - 1] == '/'){
            u = u.slice(0, u.length - 1);
        }

        var a0 = u.split('//');
        var a1 = a0.splice(1, a0.length - 1).join().split('/');
        var a2 = '/' + a1.splice(1, a1.length - 1).join('/');
        return a2;
    };
}

function FacebookPlugins() {

    this.init = function(){
        initFacebookPlugin();
    };

    var initFacebookPlugin = function () {

        //FB.api(
        //    "/853365318060222",
        //    function (r) {
        //        console.log("Got Facebook response", r);
        //
        //    }
        //);
    };


    this.init();
}

(function(){
    $(document).ready(function(){
        new Platform();
    });
})(jQuery);