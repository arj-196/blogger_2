(function($) {
  "use strict";
  var openMap = false;


  //Run function when window finished load
  //$(window).load(function() {
  //  $('#btn-map-toogle').click(function() {
  //    var target = this.hash, $target = $(target);
  //    setTimeout(function() {
  //      $('html, body').stop().animate({
  //        'scrollTop': $target.offset().top
  //      }, 300);
  //      if (!openMap) {
  //        initMapDetail();
  //      }
  //      openMap = true;
  //    }, 300);
  //    if (openMap) {
  //      return false;
  //    }
  //  });
  //});

    $(document).ready(function() {
        setTimeout(function() {
            if (!openMap) {
                initMapDetail();
            }
            openMap = true;
        }, 300);
        if (openMap) {
            return false;
        }
    });


    function initMapDetail() {
    $(function() {
        var details = $("#map-detail-job");

      if (details.length > 0) {
          var locationLat;
          var locationLng;
          if( details.data('lat') != "null"){
              locationLat = details.data('lat');
              locationLng = details .data('lng');
              buildMap(details, locationLat, locationLng)
          }else{
              var address = details.data('add');
              $.ajax({
                  url:"http://maps.googleapis.com/maps/api/geocode/json?address="+address+"&sensor=false",
                  type: "POST",
                  success:function(res){
                      console.log(" made lat lng by hand ");
                      buildMap(details, res.results[0].geometry.location.lat, res.results[0].geometry.location.lng);

                  },
                  error:function(res){
                      console.log(res);
                  }
              })
              .fail(function(){
                      console.log('Error! MAP AJAX');
                  })
              ;
          }
      }
      //$(window).off('.affix');
      //$('#affix-box').removeData('bs.affix').removeClass('affix affix-top affix-bottom');
      //initAffix();
    });
  }

  function buildMap(details, locationLat, locationLng){
      details .gmap3({
          map: {
              options: {
                  center: [locationLat, locationLng],
                  zoom: 16,
                  scaleControl: false,
                  panControl: false,
                  streetViewControl: false,
                  overviewMapControl: false,
                  zoomControl: true,
                  scrollwheel: false,
                  mapTypeId: google.maps.MapTypeId.ROADMAP,
                  mapTypeControl: false,
                  zoomControlOptions: {
                      style: google.maps.ZoomControlStyle.SMALL
                  },
                  mapTypeControlOptions: {
                      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                  }
              }
          }, marker: {
              latLng: [locationLat, locationLng],
              options: {"icon": "/bundles/app/theme/images/pinmarker.png"}
          }
      });
  }

})(jQuery);

