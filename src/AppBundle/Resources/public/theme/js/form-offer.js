/** @namespace conf.ent.optional */
/** @namespace conf.details */

var gdata = {
    poste: {
        local: [
            { "title": "Consultant UX" },
            { "title": "UX designer" },
            { "title": "Développeur iOS" },
            { "title": "Développeur Android" },
            { "title": "Développeur Javascript" },
            { "title": "Chef de projet technique" },
            { "title": "Consultant SEO" },
            { "title": "Développeur Drupal" },
            { "title": "Développeur mobile" },
            { "title": "User intelligence" },
            { "title": "Ingénieur J2EE" },
            { "title": "Développeur Symfony" },
            { "title": "Intégrateur HTML" }
        ]
    }
};

var conf = {};
var typingTimer;   //timer identifier
var doneTypingInterval = 500;

function FormOffer(options){
    var defaults = {
        fn: {}
    };
    $.extend(conf, defaults, options);

    this.init = function(){
        $(document).ready(function(){
            if(conf.ent.optional.description.val().trim() == ""){
                conf.ent.optional.description.val("");
                conf.details.mission.val("");
                conf.details.profile.val("");
            }
            conf.ent.val.val(null);
        });
    };


    // main --
    //conf.fn.ent = new EntList();
    conf.fn.posteAutocompleter = new PosteAutcompleter();
    conf.fn.enterpriseAutocompleter = new EnterpriseAutcompleter();
    this.init();

    l(conf);
}

function PosteAutcompleter(){
    var self = this;

    this.init = function(){
        buildAutocompleter();
    };

    var buildAutocompleter = function(){
        var data = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('title'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            //prefetch: conf.poste.prefetch
            local: gdata.poste.local
        });

        conf.poste.input.typeahead(null, {
            name: 'poste',
            display: 'title',
            source: data
            , templates: {
                empty: [
                    '<a href="'+ conf.url.contact +'"><div class="ac-l">',
                    'Pas de résultat : suggérer un nouveau métier ',
                    '</div></a>'
                ].join('\n'),
                suggestion: Handlebars.compile(
                    '<div class="ac-l" data-id="{{ id }}"><div><strong>{{ title }}</div></div>'
                )
            }
        });

        // checking if value is correct
        conf.poste.input.focusout(self.isValid);
    };

    this.isValid = function(next){
        var status = false;
        var text = conf.poste.input.val();
        gdata.poste.local.forEach(function(d){
            if(text.toLowerCase() === d.title.toLowerCase())
                status = true;
        });

        if(!status){
            conf.poste.input.val("").text("");
            if(text.trim() != "")
                alert("Métier: choose one of the options from the list");
        }
    };

    // main --
    this.init();
}

function EnterpriseAutcompleter(){
    var self = this;

    this.init = function(){
        buildAutocompleter();
        bindListeners();
    };

    var buildAutocompleter = function(){
        var data = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('title'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: conf.enterprise.remote + '/%QUERY',
                wildcard: '%QUERY'
            }
        });

        conf.enterprise.input.typeahead(null, {
            name: 'enterprise',
            display: 'name',
            source: data
            , templates: {
                empty: [
                    '<div class="ac-l">',
                    'Pas de résultat',
                    '</div>'
                ].join('\n'),
                suggestion: Handlebars.compile(
                    '<div class="ac-l" data-id="{{ id }}"><div><strong>{{ name }}</div></div>'
                )
            }
        });
    };

    var bindListeners = function(){
        conf.enterprise.input.on('typeahead:change', function(){
            var text = conf.enterprise.input.val();
            $.ajax({
                method: 'POST'
                , url: conf.enterprise.getEnterprise
                , data: {
                    name: text
                },
                dataType: "json",
                success: function (r) {
                    l(r);
                    if(r.e){
                        conf.details.description.val(r.e.description);
                        conf.ent.val.val(r.e.id);
                        if(r.e.a){
                            conf.details.street.val(r.e.a.street);
                            conf.details.postal.val(r.e.a.postal);
                            conf.details.city.val(r.e.a.city);
                            conf.details.country.val(r.e.a.country);
                        }
                        if(r.e.p){
                            conf.details.phone.val(r.e.p.phone);
                        } else {
                            conf.details.phone.val("");
                        }
                        if(r.e.e){
                            conf.details.email.val(r.e.e.email);
                        } else {
                            conf.details.email.val("");
                        }
                    } else {
                        conf.details.description.val("");
                        conf.details.email.val("");
                        conf.details.phone.val("");
                        conf.details.street.val("");
                        conf.details.postal.val("");
                        conf.details.city.val("");
                        conf.details.country.val("");

                    }
                }
            })
        });
    };

    // main --
    this.init();
}



// DEPRECIATED
function EntList(){
    this.init = function(){

        // Enterprise Click
        $.each(conf.ent.container.find(conf.ent.item), function () {
            $(this).on('click', enterpriseClick);
        });

        // Search Filter
        conf.ent.search.input.on("keyup", function(e){
            if(e.which == 13) {
                e.preventDefault();
            }
            //on keyup, start the countdown
            clearTimeout(typingTimer);
            typingTimer = setTimeout(filterList, doneTypingInterval);
        });

        //on keydown, clear the countdown
        conf.ent.search.input.on('keydown', function () {
            clearTimeout(typingTimer);
        });
    };

    var enterpriseClick = function(){
        l($(this).data());
        var data = $(this).data();
        conf.ent.val.val(data.id);
        conf.details.street.val(data.street);
        conf.details.postal.val(data.postal);
        conf.details.city.val(data.city);
        conf.details.country.val(data.country);
        conf.details.phone.val(data.phone);
        conf.details.email.val(data.email);

        conf.ent.result.empty();
        conf.ent.result.append(
            "<div class='pf-ent-result cursor-remove'>" +
                "<label class='cursor-remove'>"+ data.name +"</label>" +
            "</div>"
        );
        conf.ent.optional.type.prop('disabled', true);
        conf.ent.optional.name.val("").prop('disabled', true);
        conf.ent.optional.description.prop('disabled', true);
        conf.ent.optional.description.parent().css('display', 'none');
        bindRemoveEntClick();
    };

    var bindRemoveEntClick = function(){
        $.each($('.pf-ent-result'), function(){
            $(this).on('click', enterpriseRemoveClick);
        });
    };

    var enterpriseRemoveClick = function(){
        conf.ent.result.empty();
        conf.ent.val.val("null");
        conf.ent.optional.type.prop('disabled', false);
        conf.ent.optional.name.prop('disabled', false);
        conf.ent.optional.description.prop('disabled', false);
        conf.ent.optional.description.parent().css('display', 'block');
        conf.details.street.val("");
        conf.details.postal.val("");
        conf.details.city.val("");
        conf.details.country.val("France");
        conf.details.phone.val("");
        conf.details.email.val("");
    };

    var filterList = function(){
        var value = conf.ent.search.input.val().trim().toLowerCase();
        $.each(conf.ent.container.find(conf.ent.item), function(){
            var valEmpty = -1;
            if(value === ""){
                valEmpty = null; // allow everything
            }
            var text = $(this).text().trim().toLowerCase();
            if(text.search(value) != valEmpty){
                $(this).removeClass('hide');
            } else {
                $(this).addClass('hide');
            }
        });
    };

    // -- main
    this.init();
}