(function () {
    var conf = {};
    function SearchBar(options){
        var defaults = {
            query: {
                prefetch: '/tmp/search_cache.json',
                remote: global.search.remote
            },
            search: {
                url: '/search/query/'
            }
        };
        $.extend(conf, options, defaults);

        // trigger open/close
        conf.trigger.on('click', function () {
            if(conf.container.hasClass('is-visible'))
                conf.container.removeClass('is-visible');
            else
                conf.container.addClass('is-visible');
        });

        conf.input.on('keypress', function (e) {
            if(e.which == 13){
                validateSearch(conf.input.val().trim());
            }
        });

        conf.btn.on('click', function () {
            validateSearch(conf.input.val().trim());
        });

        // initialising typeahead
        this.typeahead = function(){

            var data = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: conf.query.prefetch,
                remote: {
                    url: conf.query.remote + '/%QUERY',
                    wildcard: '%QUERY'
                }
            });

            conf.input.typeahead(null, {
                name: 'states',
                display: 'value',
                source: data
                , templates: {
                    empty: [
                        '<div class="ac-l">',
                        'Pas de résultat',
                        '</div>'
                    ].join('\n'),
                    //header: '<h3 class="league-name">NBA Teams</h3>',
                    suggestion: Handlebars.compile(
                        '<a href="'+ conf.path +'{{ id }}/' + ' xxx '+ '">' +
                            '<div class="ac-l" data-id="{{ id }}"><div><strong>{{ title }}</div></div>' +
                        '</a>'
                    )
                }
            });
        };

        // -- main
        this.typeahead();
        // show search bar on default
        //conf.container.addClass('is-visible');


        var validateSearch = function (query) {
            var valid = true;
            console.log("Enter", query);

            if(valid)
                doSearch(query);
        };

        var doSearch = function (query){
            $.ajax({
                method: 'POST'
                , url: conf.search.url
                , data: {
                    query: query
                },
                dataType: "json",
                success: function (r) {
                    if (r.status) {
                        console.log("got response", r);
                    }
                }
            })
        };

    }

    $(document).ready(function () {
        new SearchBar({
            container: $('#navbar-search-container'),
            input: $('#navbar-search-input'),
            btn: $('#navbar-search-btn'),
            trigger: $('#btn-search-navbar-trigger'),
            path: global.path.paths.offer.split(global.path.splitAt)[0]
        });
    })
})(jQuery, Bloodhound, Handlebars);