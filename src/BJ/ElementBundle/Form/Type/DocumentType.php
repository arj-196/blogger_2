<?php

namespace BJ\ElementBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocumentType extends  AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName','text')
            ->add('lastName','text')
            ->add('email','email')
            ->add('file')
            ->add('offer_id','hidden')
            ->add('text','textarea')
        ;
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BJ\ElementBundle\Entity\Document'
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'bj_element_document';
    }


}