<?php

namespace BJ\ElementBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddressType  extends  AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('street','text')
            ->add('postal','text')
            ->add('city','text')
            ->add('country','text')
            ->add('comment','text',array(
                'required'=>false
            ))
            ->add('update','submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BJ\ElementBundle\Entity\Address'
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'bj_element_address';
    }

}