<?php

namespace BJ\ElementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\SerializerBuilder;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity
 */
class Address
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Complementary data of the “delivery point”: stairs, apartment, floor, at somebody’s house, etc.
     *
     * @var string
     * @ORM\Column(name="complementary_home", type="string", nullable=true)
     */
    private $complementaryHome;

    /**
     * Complementary data on the location: building, residence, etc.
     *
     * @var string
     *
     * @ORM\Column(name="complementry_location", type="string", nullable=true)
     */
    private $complementaryLocation;

    /**
     * Distribution service, complementary data on the street location (BP, lieu-dit...).
     *
     * @var string
     *
     * @ORM\Column(name="complementary_street", type="string", length=255, nullable=true)
     */
    private $complementaryStreet;

    /**
     * Street number + bis, ter... + kind of street + street name
     *
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * Postal or Cedex code.
     *
     * @var string
     *
     * @ORM\Column(name="postal", type="string", length=20, nullable=true)
     */
    private $postal;

    /**
     * City name.
     *
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * Country name.
     *
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=32, nullable=true)
     */
    private $country;
    /**
     * Country name.
     *
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @param string|null $address Address
     */
    function __construct($street=null,$city=null,$country=null,$postal=null) {

        if (!is_null($street)) {
            $this->street = $street;
        }
        if (!is_null($city)) {
            $this->city = $city;
        }
        if (!is_null($country)) {
            $this->country = $country;
        }
        if (!is_null($postal)) {
            $this->postal = $postal;
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set complementaryHome
     *
     * @param string $complementaryHome
     *
     * @return Address
     */
    public function setComplementaryHome($complementaryHome) {

        $this->complementaryHome = $complementaryHome;

        return $this;
    }

    /**
     * Get complementaryHome
     *
     * @return string
     */
    public function getComplementaryHome() {

        return $this->complementaryHome;
    }

    /**
     * Set complementaryLocation
     *
     * @param string $complementaryLocation
     *
     * @return Address
     */
    public function setComplementaryLocation($complementaryLocation) {

        $this->complementaryLocation = $complementaryLocation;

        return $this;
    }

    /**
     * Get complementaryLocation
     *
     * @return string
     */
    public function getComplementaryLocation() {

        return $this->complementaryLocation;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Address
     */
    public function setStreet($street) {

        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet() {

        return $this->street;
    }

    /**
     * Set complementaryStreet
     *
     * @param string $complementaryStreet
     *
     * @return Address
     */
    public function setComplementaryStreet($complementaryStreet) {

        $this->complementaryStreet = $complementaryStreet;

        return $this;
    }

    /**
     * Get complementaryStreet
     *
     * @return string
     */
    public function getComplementaryStreet() {

        return $this->complementaryStreet;
    }

    /**
     * Set postal
     *
     * @param integer $postal
     *
     * @return Address
     */
    public function setPostal($postal) {

        $this->postal = $postal;

        return $this;
    }

    /**
     * Get postal
     *
     * @return integer
     */
    public function getPostal() {

        return $this->postal;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Address
     */
    public function setCity($city) {

        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity() {

        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry() {

        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country) {

        $this->country = $country;

        return $this;
    }


    /**
     * @param string $complementaryHome
     */
    public function resetComplementaryHome() {

        $this->complementaryHome = null;

        return $this;
    }


    /**
     * @param string $complementaryStreet
     */
    public function resetComplementaryStreet() {

        $this->complementaryStreet = null;

        return $this;
    }


    /**
     * @param string $complementaryLocation
     */
    public function resetComplementaryLocation() {

        $this->complementaryLocation = null;

        return $this;
    }


    /**
     * @return string
     */
    public function __toString() {
        $serializer = SerializerBuilder::create()
            ->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy())
            ->build();

        return $serializer->serialize($this, 'json');
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Address
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}
