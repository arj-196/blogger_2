<?php

namespace BJ\ElementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\SerializerBuilder;

/**
 * Email
 *
 * @ORM\Table(name="email")
 * @ORM\Entity
 */
class Email {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=254, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @param string|null $email Email address
     */
    function __construct($email = null) {

        if (!is_null($email)) {
            $this->email = $email;
        }
    }

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email) {

        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string|null
     */
    public function getEmail() {

        return $this->email;
    }

    /**
     * Set comment
     *
     * @param string|null $comment
     *
     * @return $this
     */
    public function setComment($comment) {

        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment() {

        return $this->comment;
    }

    /**
     * Reset comment to default value
     *
     * @return $this
     */
    public function resetComment() {

        $this->comment = null;

        return $this;
    }

    /**
     * @return string JSON object
     */
    public function __toString() {

        $serializer = SerializerBuilder::create()
            ->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy())
            ->build();

        return $serializer->serialize($this, 'json');
    }
}
