<?php

namespace BJ\ElementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\SerializerBuilder;

/**
 * Phone
 *
 * @ORM\Table(name="phone")
 * @ORM\Entity
 */
class Phone {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=20, nullable=true)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


    /**
     * @param string|null $number Phone number
     */
    function __construct($number = null) {

        if (!is_null($number)) {
            $this->number = $number;
        }
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Phone
     */
    public function setNumber($number) {

        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber() {

        return $this->number;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Phone
     */
    public function setComment($comment) {

        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment() {

        return $this->comment;
    }

    /**
     * Reset comment to default value.
     *
     * @return $this
     */
    public function resetComment() {

        $this->comment = null;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {

        $serializer = SerializerBuilder::create()
            ->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy())
            ->build();

        return $serializer->serialize($this, 'json');
    }
}
