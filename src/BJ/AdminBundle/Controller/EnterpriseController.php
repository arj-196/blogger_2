<?php

namespace BJ\AdminBundle\Controller;

use BJ\CorporateBundle\Entity\Offer;
use BJ\CorporateBundle\Entity\Tracker;
use BJ\CorporateBundle\Form\Type\ContractType;
use BJ\CorporateBundle\Form\Type\EnterpriseType;
use BJ\CorporateBundle\Form\Type\OfferType;
use BJ\ElementBundle\Entity\Address;
use BJ\ElementBundle\Entity\Email;
use BJ\ElementBundle\Entity\Phone;
use BJ\ElementBundle\Form\Type\AddressType;
use BJ\ElementBundle\Form\Type\PhoneType;
use BJ\ElementBundle\Form\Type\EmailType;
use BJ\ElementBundle\Entity\Position;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EnterpriseController extends Controller{

    /**
     * Enterprise Dashboard
     * @return Response
     */
    public function indexAction(){
        $user = $this->getUser();

        $settings = [
            'action'=>'overview',
            'user'=>$user
        ];

        return $this->render('BJAdminBundle:Enterprise:dashboard_overview.html.twig',array(
            'settings'=>$settings
        ));
    }

    /**
     * Viewing Enterprise Details
     * @param $count
     * @return Response
     */
    public function enterpriseAction($count){
        //TODO delete enterprise
        $user = $this->getUser();
        $enterprise = $user->getEnterprises()[$count];
        $offers = $enterprise->getOffers();

        $settings = [
            'action'=>'enterprise',
            'user'=>$user,
            'enterprise' => $enterprise,
            'offers'=>$offers,
            'count' => $count
        ];

        return $this->render('BJAdminBundle:Enterprise:dashboard_overview.html.twig',array(
            'settings'=>$settings
        ));
    }

    /**
     * Editing Enterprise
     * @param Request $request
     * @param $count
     * @return Response
     */
    public function enterpriseEditAction(Request $request, $count){
        $user = $this->getUser();
        $enterprise = $user->getEnterprises()[$count];
        $formEnt = $this->createForm(new EnterpriseType(), $enterprise);

        $formEnt->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if($formEnt->isValid()){
            $data = $formEnt->getData();
            $em->persist($data);
            $em->flush();

        }


        $settings = [
            'action'=>'enterprise_edit',
            'user'=>$user,
            'enterprise' => $enterprise,
            'count'=>$count
        ];

        return $this->render('BJAdminBundle:Enterprise:dashboard_overview.html.twig',array(
            'settings'=>$settings,
            'form'=>$formEnt->createView(),
        ));
    }

    /**
     * Create Enterprise
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function enterpriseNewAction(Request $request){
        $user = $this->getUser();
        $form = $this->createForm(new EnterpriseType());
        $form->handleRequest($request);

        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $user->addEnterprise($data);

            $em->persist($data);
            $em->persist($user);
            $em->flush();
            $em->refresh($user);
            $count = $user->getEnterprises()->count();

            return $this->redirect(
                $this->generateUrl('user.enterprise.dashboard.edit.enterprise',array('count'=>$count-1)
            ));
        }

        $settings = [
            'action'=>'enterprise_new',
            'user'=>$user,
        ];

        return $this->render('BJAdminBundle:Enterprise:dashboard_overview.html.twig',array(
            'settings'=>$settings,
            'form'=>$form->createView()
        ));
    }

    /**
     * Removing Enterprise
     * @param $count
     * @param $offerCount
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function enterpriseRemoveAction($count){
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $enterprise = $user->getEnterprises()[$count];

        $user->removeEnterprise($enterprise);
        foreach($enterprise->getOffers() as $offer ){
            $enterprise->removeOffer($offer);
            $em->remove($offer);
        }

        $em->remove($enterprise);
        $em->flush();

        return $this->redirect(
            $this->generateUrl('user.enterprise.dashboard')
        );
    }

    /**
     * Editing Offer
     * @param Request $request
     * @param $count
     * @param $offerCount
     * @return Response
     */
    public function offerEditAction(Request $request, $count, $offerCount){
        $user = $this->getUser();
        $enterprise = $user->getEnterprises()[$count];
        $offer = $enterprise->getOffers()[$offerCount];

        $form = $this->createForm(new OfferType(), $offer);

        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if($form->isValid()){
            $data = $form->getData();
            $em->persist($data);
            $em->flush();

//            var_dump($data);
        }

        $settings = [
            'action'=>'offer_edit',
            'user'=>$user,
            'enterprise' => $enterprise,
            'offer'=>$offer,
            'count' => $count
        ];

        return $this->render('BJAdminBundle:Enterprise:dashboard_overview.html.twig',array(
            'settings'=>$settings,
            'form'=>$form->createView(),
        ));
    }

    /**
     * Removing Offer
     * @param $count
     * @param $offerCount
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function offerRemoveAction($count, $offerCount){
        $user = $this->getUser();
        $enterprise = $user->getEnterprises()[$count];
        $offer = $enterprise->getOffers()[$offerCount];
        $enterprise->removeOffer($offer);
        $em = $this->getDoctrine()->getManager();
        $em->remove($offer);
        $em->flush();
        $em->refresh($enterprise);

        return $this->redirect(
            $this->generateUrl('user.enterprise.dashboard.view.enterprise',array('count'=>$count)
            ));
    }

    /**
     * Creating new Offer
     * @param Request $request
     * @param $count
     * @param $offerCount
     * @return Response
     */
    public function offerNewAction(Request $request, $count){
        $user = $this->getUser();
        $enterprise = $user->getEnterprises()[$count];
        $offer = new Offer();

        $form = $this->createForm(new OfferType(), $offer);
        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $data->setEnterprise($enterprise);
            $tracker = new Tracker();
            $tracker->setOffer($offer);

            if(is_null($data->getDuration()))
                $data->setDuration('3');
            if(is_null($data->getPrivacy()))
                $data->setPrivacy(0);
            if(is_null($data->getType()))
                $data->setType('stage');

            $em->persist($tracker);
            $em->persist($data);
            $em->flush();
            $em->refresh($data);

            return $this->redirect($this->generateUrl(
                'user.enterprise.dashboard.view.enterprise',array('count'=>$count )
            ));

            // Depreciated
            //return $this->redirect($this->generateUrl(
            //    'user.enterprise.dashboard.new.offer.contract',array('count'=>$count ,'offerCount'=> $data->getId() )
            //));

        }

        $settings = [
            'action'=>'offer_new_stage_1',
            'user'=>$user,
            'enterprise' => $enterprise,
            'offer'=>$offer,
            'count' => $count
        ];

        return $this->render('BJAdminBundle:Enterprise:dashboard_overview.html.twig',array(
            'settings'=>$settings,
            'form'=>$form->createView()
        ));

    }

    /**
     * DEPRECIATED
     * Create new Contract for Offer
     * @param Request $request
     * @param $count
     * @param $offerCount
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function offerNewContractAction(Request $request, $count, $offerCount){
        $user = $this->getUser();
        $enterprise = $user->getEnterprises()[$count];

        $offer = $this->getDoctrine()->getRepository('BJCorporateBundle:Offer')
            ->find($offerCount);

        $formC = $this->createForm(new ContractType());

        $formC->handleRequest($request);


        if($formC->isValid()){
            $em = $this->getDoctrine()->getManager();
            $data = $formC->getData();
            $offer->setContract($data);
            $em->persist($offer);
            $em->flush();

            return $this->redirect($this->generateUrl(
                'user.enterprise.dashboard.view.enterprise',array('count'=>$count )
            ));
        }

        $settings = [
            'action'=>'offer_new_stage_2',
            'user'=>$user,
            'enterprise' => $enterprise,
            'offer'=>$offer,
            'count' => $count
        ];

        return $this->render('BJAdminBundle:Enterprise:dashboard_overview.html.twig',array(
            'settings'=>$settings,
            'formC'=>$formC->createView()
        ));
    }

}