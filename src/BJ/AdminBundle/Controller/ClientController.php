<?php

namespace BJ\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClientController extends Controller {

    public function indexAction(){
        $user = $this->getUser();
        $engine = $this->get('administration.super');

        $settings = [
            'action'=>'overview',
            'user'=>$user
        ];

        return $this->render('BJAdminBundle:Client:dashboard_overview.html.twig',array(
            'settings'=>$settings
        ));
    }
}