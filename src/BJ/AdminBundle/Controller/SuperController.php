<?php

namespace BJ\AdminBundle\Controller;

use BJ\CorporateBundle\Form\Type\ContractType;
use BJ\CorporateBundle\Form\Type\OfferType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SuperController extends Controller{

    /**
     * Super Admin Dashboard
     * @return Response
     */
    public function indexAction(){
        $user = $this->getUser();
        $engine = $this->get('administration.super');

        $global_info = $engine->getGlobalInfo();

        $settings = [
            'action'=>'overview',
            'user'=>$user,
            'global'=>$global_info
        ];

        return $this->render('BJAdminBundle:SuperAdmin:dashboard_overview.html.twig',array(
            'settings'=>$settings
        ));
    }

    /**
     * Dashboard Options
     * @param $option
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function optionAction($option){
        if($option == 'clear_cache'){
            $engine = $this->get('administration.super');
            $engine->createCache();
        }
        else if($option == 'remove_old_newsletter'){
            $engine = $this->get('administration.super');
            $engine->removeOutdatedOffers();
        }

//        return new Response('Dashboard options '.$option);
        return $this->redirect($this->generateUrl('user.admin.super.dashboard'));
    }

    /**
     * Listing Newsletters
     * @return Response
     */
    public function newsletterAction(){
        $engine = $this->get('administration.super');
        $letters = $engine->getNewsletters();

        return $this->render('BJAdminBundle:SuperAdmin/Component:newsletter_list.html.twig',array(
            'letters'=>$letters
        ));
    }

    public function authoriseAction(Request $request, $id){
        $offer = $this->getDoctrine()->getRepository('BJCorporateBundle:Offer')
            ->find($id);

        $authorise = $request->request->get('authorise');
        if(!is_null($authorise)){
            $em = $this->getDoctrine()->getManager();
            $offer->getTracker()->setAuthorised($authorise);
            $em->persist($offer);
            $em->flush();

            if($authorise == '1'){
                $mailer = $this->get("mailgun_mail");
                $mailer->send_mail(
                    $offer->getEmail()->getEmail(),
                    'UNJOBDANSLAPUB : Vous avez bien mis en ligne votre offre',
                    $this->renderView(
                        ':Emails:job_upload.html.twig', array(
                            'offer' => $offer
                        )
                    ),
                    null
                );
            }
        }

        return $this->render("BJAdminBundle:SuperAdmin/Component:authorise.html.twig",array(
            'offer'=>$offer
        ));
    }

    /**
     * Offer lists
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function listAction($action, $iteration){

        $engine = $this->get('administration.super');
        $max=30;
        $offers=$engine->getOffers($action, $iteration, $max);

        $options = $this->get('search_engine')->filterOptions();
        $settings=[
            'action'=>$action,
            'paginate'=>$iteration,
            'path'=>[
                'previous'=>$this->generateUrl('user.admin.super.dashboard.list',array(
                    'action'=>$action,
                    'iteration'=>$iteration-1
                )),
                'next'=>$this->generateUrl('user.admin.super.dashboard.list',array(
                    'action'=>$action,
                    'iteration'=>$iteration+1
                ))
            ],
            'param'=>[
                'iterationPlus'=>1
            ]
        ];
        return $this->render('BJAdminBundle:SuperAdmin/Component:offer_list.html.twig',array(
            'offers'=>$offers,
            'settings'=>$settings,
            'options'=>$options
        ));
    }

    /**
     * Getting Offers for a particular enterprise
     * @param Request $request
     * @return Response
     */
    public function filterListAction(Request $request){

        $agency = $request->get('enterprise');
        $options = $this->get('search_engine')->filterOptions();
        $offers = $this->getDoctrine()->getManager()
            ->createQuery(
                ' SELECT o'.
                ' FROM BJCorporateBundle:Offer o'.
                ' JOIN o.enterprise e'.
                ' WHERE e.name = :agency'.
                ' ORDER BY o.createdAt DESC'
            )
            ->setParameter('agency', $agency)
            ->getResult()
        ;

        $settings=[];
        return $this->render('BJAdminBundle:SuperAdmin/Component:offer_list_filter.html.twig',array(
            'offers'=>$offers,
            'settings'=>$settings,
            'options'=>$options
        ));

    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function offerEditAction(Request $request, $id){
        $offer = $this->getDoctrine()->getRepository('BJCorporateBundle:Offer')
            ->find($id);

        $form = $this->createForm(new OfferType(), $offer);

        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if($form->isValid()){
            $data = $form->getData();
            $em->persist($data);
            $em->flush();
        }

        $settings = [
            'action'=>'offer_edit',
            'offer'=>$offer,
        ];

        return $this->render('BJAdminBundle:SuperAdmin/Component:offer_edit.html.twig',array(
            'settings'=>$settings,
            'form'=>$form->createView()
        ));
    }

    /**
     * Removing an Offer
     * @param Request $request
     * @param $id
     */
    public function offerRemoveAction(Request $request, $id){
        //TODO finish
    }

    /**
     * Depreciated
     * Fetching Offer lists
     * Ajax Implementation
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function indexAjaxAction(Request $request){
        if($request->isXmlHttpRequest()){
            $engine = $this->get('administration.super');
            $engine->createCache();

            $action = $request->get('action');
            $offers=$engine->getOffersPerAjax($action);

            $html = $this->render('BJAdminBundle:SuperAdmin/Ajax:offer_container.html.twig',array(
                'offers'=>$offers
            ));


            return new JsonResponse(array(
                'content'=>$html->getContent()
            ));
        }

        $engine = $this->get('administration.super');
        $engine->createCache();
        $action = 'offer_new_auth';
        $offers=$engine->getOffersPerAjax($action);

        return $this->render('BJAdminBundle:SuperAdmin/Ajax:offer_container.html.twig',array(
            'offers'=>$offers
        ));
    }


}