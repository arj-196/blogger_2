<?php

namespace BJ\UserBundle\Entity;

use BJ\CorporateBundle\Entity\Enterprise;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="BJ\CorporateBundle\Entity\Enterprise")
     * @ORM\JoinTable(name="user_enterprises",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="enterprise_id", referencedColumnName="id", unique=false)}
     *      )
     **/
    protected $enterprises;


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->enterprises = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Check if enterprise exist already
     * @param Enterprise $target
     * @return bool
     */
    public function existEnterprise(Enterprise $target){
        foreach($this->enterprises as $enterprise){
            if($enterprise->getId() == $target->getId()){
                return true;
            }
        }
        return false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }



    /**
     * Add enterprises
     *
     * @param \BJ\CorporateBundle\Entity\Enterprise $enterprises
     * @return User
     */
    public function addEnterprise(\BJ\CorporateBundle\Entity\Enterprise $enterprises)
    {
        $this->enterprises[] = $enterprises;

        return $this;
    }

    /**
     * Remove enterprises
     *
     * @param \BJ\CorporateBundle\Entity\Enterprise $enterprises
     */
    public function removeEnterprise(\BJ\CorporateBundle\Entity\Enterprise $enterprises)
    {
        $this->enterprises->removeElement($enterprises);
    }

    /**
     * Get enterprises
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEnterprises()
    {
        return $this->enterprises;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
}
