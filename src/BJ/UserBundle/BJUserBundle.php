<?php

namespace BJ\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BJUserBundle extends Bundle
{

    /**
     * Defines this Bundle as a child of the FOSUserBundle.
     *
     * @return string Name of the parent bundle.
     */
    public function getParent() {

        return 'FOSUserBundle';
    }

}
