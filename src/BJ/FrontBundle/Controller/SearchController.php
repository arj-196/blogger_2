<?php

namespace BJ\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller
{

    public function indexAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $engine = $this->get('search_site');
            $query = $request->get('query');

            $res = $engine->doSearch($query);

            return new JsonResponse(array(
                'status' => true,
                'r' => $res,
                'query' => $query
            ));
        }

        return null;
    }

    public function suggestionAction(Request $request, $query)
    {
        $engine = $this->get('search_site');

        return new JsonResponse($engine->getSuggestions($query));
    }

    public function enterpriseSuggestionAction(Request $request, $query)
    {
        $engine = $this->get('search_site');
        return new JsonResponse($engine->getEnterpriseSuggestions($query));
    }

    public function getEnterpriseAction(Request $request)
    {
        $engine = $this->get('search_site');
        if ($request->isXmlHttpRequest()) {
            $name = $request->get('name');
            $data = $engine->getEnterpriseByName($name);
            return new JsonResponse(array("status" => true, "e" => $data));
        }


        $name = "Bold+Beyond";
        $data = $engine->getEnterpriseByName($name);

        return new JsonResponse(array("status" => true, "e" => $data));
    }

}