<?php

namespace BJ\FrontBundle\Controller;

use BJ\ElementBundle\Entity\Document;
use BJ\ElementBundle\Form\Type\DocumentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * Landing Page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        $engine = $this->get('search_engine');
        $option = $engine->filterOptions();

        $settings = [
            'members' =>
                count(
                    $this->get('fos_user.user_manager')
                        ->findUsers()
                ),
            'offers' =>
                $this->getDoctrine()
                    ->getRepository('BJCorporateBundle:Offer')
                    ->findOfferCount(),
            'enterprises' =>
                $this->getDoctrine()
                    ->getRepository('BJCorporateBundle:Enterprise')
                    ->findEnterpriseCount(),
            'results' =>
                $this->getDoctrine()
                    ->getRepository('BJCorporateBundle:Offer')
                    ->findOffersByContractTitle($option['contracts']),
            'option' => $option
        ];

        return $this->render('BJFrontBundle:Default:index.html.twig', array(
            'settings' => $settings
        ));
    }

    /**
     * Offer Details
     * Submitting Application
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function offerAction($id, $title, Request $request)
    {
        $offer = $this->getDoctrine()
            ->getRepository('BJCorporateBundle:Offer')
            ->find($id);

        $document = new Document();

        $form = $this->createForm(new DocumentType(), $document, array(
            'action' => $this->generateUrl('bj_offer_details', array(
                'id' => $id,
                'title' => $title
            )),
            'method' => 'POST',
        ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $offer = $this->getDoctrine()->getRepository('BJCorporateBundle:Offer')
                ->find($document->getOfferId());
            $em = $this->getDoctrine()->getManager();

            $em->persist($document);
            $em->flush();
            $em->refresh($document);

            $attachment = null;
            if (!is_null($document->getAbsolutePath())) {
                $attachment = $document->getAbsolutePath();
            }


            $mailer = $this->get("mailgun_mail");
            $mailer->send_mail(
                $offer->getEmail()->getEmail(),
                'UNJOBDANSLAPUB : quelqu\'ya a postuler pour votre offre!',
                $this->renderView(
                    ':Emails:job_application.html.twig', array(
                        'document' => $document,
                        'offer' => $offer
                    )
                ),
                $attachment
            );

        }

        return $this->render('BJFrontBundle:Default:job_details.html.twig', array(
            'offer' => $offer,
            'form' => $form->createView(),
            'config' => [
                'title' => $offer->getTitle()
            ]
        ));
    }


    /**
     * listing enterprise Alphabetically
     * @param $type
     * @return Response
     */
    public function enterpriseListingAction($type)
    {
        $results = [];
        $rep = $this->getDoctrine()->getManager();

        $gloabal_count = $rep->createQuery(
            ' SELECT count(e)' .
            ' FROM BJCorporateBundle:Enterprise e' .
            ' WHERE e.type = :type'
        )
            ->setParameters(array(
                'type' => strtolower($type)
            ))
            ->getResult()[0][1];

        $alphabets = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
        foreach ($alphabets as $k => $a) {
            $enterprises = $rep->createQuery(
                ' SELECT e' .
                ' FROM BJCorporateBundle:Enterprise e' .
                ' WHERE e.type = :type' .
                ' AND e.name like :name' .
                ' ORDER BY e.name '
            )
                ->setParameters(array(
                    'type' => strtolower($type),
                    'name' => $a . '%'
                ))
                ->getResult();;

            $count = [];
            foreach ($enterprises as $e) {
                $c = $rep->createQuery(
                    ' SELECT count(o)' .
                    ' FROM BJCorporateBundle:Offer o' .
                    ' JOIN o.enterprise e' .
                    ' JOIN o.tracker t ' .
                    ' WHERE e.id = :id' .
                    ' AND t.authorised = :auth' .
                    ' AND o.privacy = 0'
                )
                    ->setParameters(array(
                        'id' => $e->getId(),
                        'auth' => true
                    ))
                    ->getResult();
                $count[$e->getId()] = $c[0][1];
            }


            $results[] = [
                'a' => $a,
                'enterprises' => $enterprises,
                'count' => $count
            ];
        }
        return $this->render('BJFrontBundle:Default:enterprise_listing.html.twig', array(
            'results' => $results,
            'alphabets' => $alphabets,
            'type' => $type,
            'global_count' => $gloabal_count
        ));
    }

    /**
     * Listing jobs for enterprise
     * @param $id
     * @return Response
     */
    public function enterpriseOffersAction($id)
    {
        $enterprise = $this->getDoctrine()
            ->getRepository('BJCorporateBundle:Enterprise')
            ->find($id);
        $offers = $this->getDoctrine()
            ->getRepository('BJCorporateBundle:Enterprise')
            ->findOffersByEnterprise($id);

        $settings = [
            'enterprise' => $enterprise,
            'offers' => $offers
        ];
        return $this->render('BJFrontBundle:Default:enterprise_offer_listing.html.twig', array(
            'settings' => $settings,
            'config' => [
                'title' => $enterprise->getName() . ' : unjobdanslapub'
            ]
        ));
    }

    /**
     * Contact Page
     * @return Response
     */
    public function contactAction()
    {
        return $this->render('BJFrontBundle:Default:contact.html.twig');
    }

    // ---------------------------------------------------
    // ------------- DEPRECIATED -------------------------
    // ---------------------------------------------------

    /**
     * Test
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function testAction()
    {
        return $this->render('BJFrontBundle:Test:test.html.twig');
    }

    /**
     * Depreciated
     * Listing Enterprises
     * @return Response
     */
    public function enterprisePaginateAction($paginate)
    {

        $results = $this->getDoctrine()->getRepository('BJCorporateBundle:Enterprise')
            ->findEnterprisesPaginate($paginate);

        $settings = [
            'enterprises' => $results,
            'paginate' => $paginate
        ];

        return $this->render('BJFrontBundle:Default:enterprise_listing.html.twig', array(
            'settings' => $settings
        ));
    }

    /**
     * Depreciated
     * Listing Offers
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listingAction(Request $request)
    {

        $method = $request->getMethod();
        if ($method == 'POST') {
            $var_tags = $request->request->get('tags', '');
            $var_location = $request->request->get('location', '');
        } else {
            $var_tags = $request->get('tags');
            $var_location = $request->get('location');
        }

        $options = [
            'tags' => [],
            'location' => []
        ];

        if (!isset($var_tags)) $var_tags = "[]";
        if (!isset($var_location)) $var_location = ""; //TODO search by locatoin

        if (!empty($var_tags)) {
            $tags = json_decode($var_tags);
            $options['tags'][] = $tags;
        }

        if (!empty($var_location)) {
            $location = json_decode($var_location);
            $options['location'][] = $location;
        }

        $engine = $this->get('search_engine');
        $engine->init($options);
        $results = $engine->getResults();

        $settings = [
            'offers' => $results['offers'],
            'count' => $results['count'],
            'param' => [
                'tags' => $options['tags'][0],
                'iterationPlus' => 10
            ],
            'paginate' => 0
        ];

        //TODO if found no tags alert error
        return $this->render('BJFrontBundle:Default:job_listing.html.twig', array(
            'settings' => $settings
        ));
    }

    /**
     * Depreciated
     * Fetching data for list pagination
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function listPaginateAction(Request $request, $iteration)
    {

        //Method Get implementation
        $tags = json_decode($request->query->get('tags'));

        $offers = $this->getDoctrine()->getRepository('BJCorporateBundle:Offer')
            ->findOffersByTagPaginate($tags, $iteration - 1);
        $count = $this->getDoctrine()->getRepository('BJCorporateBundle:Offer')
            ->findOfferCountByTags($tags)[0][1];

        $settings = [
            'offers' => $offers,
            'count' => $count,
            'param' => [
                'tags' => $tags
            ],
            'paginate' => $iteration
        ];

        return $this->render('BJFrontBundle:Default:job_listing.html.twig', array(
            'settings' => $settings
        ));
    }
}