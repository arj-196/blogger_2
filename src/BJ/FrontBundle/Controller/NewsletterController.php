<?php

namespace BJ\FrontBundle\Controller;

use BJ\CorporateBundle\Entity\Newsletter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;



class NewsletterController extends Controller {

    public function registerAction(Request $request){

        $email = $request->get('email');
        $target = $request->get('target');
        $newsletter = new Newsletter();
        $newsletter->setEmail($email);
        $newsletter->setTarget($target);

        $em = $this->getDoctrine()->getManager();
        $em->persist($newsletter);
        $em->flush();

        return $this->redirect($request->headers->get('referer').'?letter=true');

    }


    public function test2Action(){

        $offers = $this->getDoctrine()->getRepository('BJCorporateBundle:Offer')
            ->findAll();
        return $this->render(':Newsletters:newsletter.html.twig',array(
            'offers'=>$offers
        ));
    }



    public function testAction(){
        set_time_limit(0);

        $api_key = '216883ec2a6d4fa60aafc9cfdedbe2d7-us10';
        $MailChimp = new \Drewm\MailChimp($api_key);

        $lists = $MailChimp->call('lists/list');
        $list_id=null;
        $letters=[];
        if($lists['total']>0){
            foreach($lists['data'] as $list){
                $letters[] = [
                    'id'=>$list['id'],
                    'type'=>$list['name'],
                ];
            }
        }

        $status=true;
        foreach($letters as $letter){
            $offers = $this->getDoctrine()->getManager()
                ->createQuery(
                    ' SELECT o'.
                    ' FROM BJCorporateBundle:Offer o'.
                    ' JOIN o.enterprise e'.
                    ' JOIN o.tracker t '.
                    ' WHERE o.type = :title'.
                    ' AND o.privacy = 0'.
                    ' AND t.authorised = :auth'.
                    ' ORDER BY o.createdAt DESC'
                )
                ->setParameters(array(
                    'title'=>$letter['type'],
                    'auth'=>true
                ))
                ->setMaxResults(5)
                ->getResult()
                ;
//            var_dump($letter);

            if(!empty($offers)){

                $html =
                    $this->renderView(
                        ':Newsletters:newsletter.html.twig',array(
                            'offers'=>$offers
                        )
                    );

                $options = array(
                    'type' => 'regular',
                    'options'=>array(
                        'list_id'=> $letter['id'],
                        'subject'=>'unjobdanslapub : '. $letter['type'],
//                        'from_email'=>'webmaster@unjobdanslapub.com',
                        'from_email'=>'arj196@hotmail.com',
                        'from_name'=>'unjobdanslapub',
                        'to_name'=>'jok seakers for '.$letter['type'],
                    ),
                    'content'=>array(
                        'html'=>$html
                    )
                );

                $campaign = $MailChimp->call('campaigns/create', $options);
//                print_r(" Creating Template ");
//                var_dump($options);
//                var_dump($campaign);

                $send= $MailChimp->call('campaigns/send',array('cid'=>$campaign['id']));
//                print_r(" Send Campaign ");
//                var_dump($send);
                if(!$send['complete']){
                    $status=false;
                }

            }
        }

        var_dump("status");
        var_dump($status);

        return new Response("NST");
    }

}