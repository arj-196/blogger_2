<?php

namespace BJ\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FilterController extends Controller
{

    /**
     * Handling Form and URL Generation
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function filterAction(Request $request)
    {

        $contract = $request->request->get('contract');
        $post = $request->request->get('post');
        $chez = $request->request->get('chez');
        $iteration = $request->request->get('iteration');

        var_dump($chez, $contract, $post, $iteration);


        // Contract - Agency - City
        if (!is_null($chez) && !is_null($contract) && !is_null($post)) {
            return $this->redirect($this->generateUrl('bj_offer_listing.contract.chez.post', array(
                'contract' => $contract,
                'chez' => $chez,
                'post' => $post
            )));
        } else {
            return $this->redirect($this->generateUrl('bj_offer_listing.contract', array(
                'contract' => 'cdd'
            )));
        }

    }

    /**
     * Contract Filter
     * @param $contract
     * @return Response
     */
    public function listingContractFilterAction($contract)
    {

        $options = [
            'path' => [
                'previous' => $this->generateUrl('bj_offer_listing.contract.paginate', array(
                    'contract' => $contract,
                    'iteration' => 0
                )),
                'next' => $this->generateUrl('bj_offer_listing.contract.paginate', array(
                    'contract' => $contract,
                    'iteration' => 1
                ))
            ],
            'newsletter' => [
                'title' => $contract,
                'target' => json_encode([
                    'contract' => $contract,
                    'agency' => null,
                    'city' => null
                ]),
                'path' => $this->generateUrl('bj_offer_newsletter')
            ]
        ];
        return $this->forward('BJFrontBundle:Filter:listingAlgorithm', array(
            'iteration' => 0,
            'contract' => $contract,
            'chez' => null,
            'post' => null,
            'options' => $options
        ));
    }

    /**
     * Contract Filter Paginate
     * @param $contract
     * @param $iteration
     * @return Response
     */
    public function listingContractFilterPaginationAction($contract, $iteration)
    {

        $options = [
            'path' => [
                'previous' => $this->generateUrl('bj_offer_listing.contract.paginate', array(
                    'contract' => $contract,
                    'iteration' => $iteration - 1
                )),
                'next' => $this->generateUrl('bj_offer_listing.contract.paginate', array(
                    'contract' => $contract,
                    'iteration' => $iteration + 1
                ))
            ],
            'newsletter' => [
                'title' => $contract,
                'target' => json_encode([
                    'contract' => $contract,
                    'agency' => null,
                    'city' => null
                ]),
                'path' => $this->generateUrl('bj_offer_newsletter')
            ]
        ];
        return $this->forward('BJFrontBundle:Filter:listingAlgorithm', array(
            'iteration' => $iteration,
            'contract' => $contract,
            'chez' => null,
            'post' => null,
            'options' => $options
        ));
    }


    /**
     *
     * Chez - Post - Contract Filter
     *
     * @param $contract
     * @param $chez
     * @param $post
     * @return Response
     */
    public function listingChezPostContractFilterAction($contract, $chez, $post)
    {
        $options = [
            'path' => [
                'previous' => $this->generateUrl('bj_offer_listing.contract.chez.post.paginate', array(
                    'contract' => $contract,
                    'chez' => $chez,
                    'post' => $post,
                    'iteration' => 0
                )),
                'next' => $this->generateUrl('bj_offer_listing.contract.chez.post.paginate', array(
                    'contract' => $contract,
                    'chez' => $chez,
                    'post' => $post,
                    'iteration' => 1
                ))
            ],
            'newsletter' => [
                'title' => $chez . " / " . $contract . ' / ' . $post,
                'target' => json_encode([
                    'contract' => $contract,
                    'chez' => $chez,
                    'post' => $post
                ]),
                'path' => $this->generateUrl('bj_offer_newsletter')
            ]
        ];

        return $this->forward('BJFrontBundle:Filter:listingAlgorithm', array(
            'iteration' => 0,
            'contract' => $contract,
            'chez' => $chez,
            'post' => $post,
            'options' => $options
        ));
    }

    /**
     * Chez - Post - Contract Filter Paginate
     * @param $contract
     * @param $chez
     * @param $post
     * @param $iteration
     * @return Response
     * @internal param $agency
     * @internal param $city
     */
    public function listingChezPostContractFilterPaginateAction($contract, $chez, $post, $iteration)
    {

        $options = [
            'path' => [
                'previous' => $this->generateUrl('bj_offer_listing.contract.chez.post.paginate', array(
                    'contract' => $contract,
                    'chez' => $chez,
                    'post' => $post,
                    'iteration' => $iteration - 1
                )),
                'next' => $this->generateUrl('bj_offer_listing.contract.chez.post.paginate', array(
                    'contract' => $contract,
                    'chez' => $chez,
                    'post' => $post,
                    'iteration' => $iteration + 1
                ))
            ],
            'newsletter' => [
                'title' => $chez . " / " . $contract . ' / ' . $post,
                'target' => json_encode([
                    'contract' => $contract,
                    'chez' => $chez,
                    'post' => $post
                ]),
                'path' => $this->generateUrl('bj_offer_newsletter')
            ]
        ];
        return $this->forward('BJFrontBundle:Filter:listingAlgorithm', array(
            'iteration' => $iteration,
            'contract' => $contract,
            'chez' => $chez,
            'post' => $post,
            'options' => $options
        ));
    }

    /**
     * Rendering Algorithm
     * @param $contract
     * @param $chez
     * @param $post
     * @param $iteration
     * @param $options
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @internal param $agency
     * @internal param $city
     */
    public function listingAlgorithmAction($contract, $chez, $post, $iteration, $options)
    {
        $max = 50;
        $offers = [];
        $count = 0;


        // Contract - Agency - City
        if (!is_null($chez) && !is_null($contract) && !is_null($post)) {

            $offers = $this->getDoctrine()
                ->getRepository('BJCorporateBundle:Offer')
                ->findOffersByChezPostContractFilter($contract, $chez, $post, $iteration, $max);

            $count = $this->getDoctrine()
                ->getRepository('BJCorporateBundle:Offer')
                ->findCountOffersByChezPostContractFilter($contract, $chez, $post);

        } else {

            $offers = $this->getDoctrine()
                ->getRepository('BJCorporateBundle:Offer')
                ->findOffersByContract($contract, $iteration, $max);

            $count = $this->getDoctrine()
                ->getRepository('BJCorporateBundle:Offer')
                ->findCountOffersByContract($contract);
        }

        $offerCountIterationPlus =
            $count - (($iteration + 1) * $max);

        if (count($offers) == 0 && $iteration != 0) {
            return $this->redirect($options['path']['previous']);
        }


        $engine = $this->get('search_engine');
        $option = $engine->filterOptions();
        $settings = [
            'offers' => $offers,
            'count' => $count,
            'param' => [
                'contract' => $contract,
                'tags' => [],
                'iterationPlus' => $offerCountIterationPlus
            ],
            'paginate' => $iteration,
            'newsletter' => $options['newsletter'],
            'path' => $options['path'],
            'filter' => true,
            'option' => $option,
            'selected' => [
                'contract' => $contract,
                'chez' => $chez,
                'post' => $post
            ]
        ];

        return $this->render('BJFrontBundle:Default:job_listing.html.twig', array(
            'settings' => $settings,
            'config' => [
                'title' => $chez . ' ' . $contract . ' ' . $post
            ]
        ));
    }

}