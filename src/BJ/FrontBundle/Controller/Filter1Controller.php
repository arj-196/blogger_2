<?php

namespace BJ\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FilterController extends Controller {

    /**
     * Handling Form and URL Generation
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function filterAction(Request $request){

        $contract = $request->request->get('contract');
        $iteration = $request->request->get('iteration');

        $agency = $request->request->get('agency');
        $agency = ( empty($agency) ) ? null :  str_replace(' ','-',$request->request->get('agency'));
        $city = $request->request->get('city');
        $city = ( empty($city) ) ? null :  str_replace(' ','-',$request->request->get('city'));

        var_dump($contract, $agency, $city, $iteration);


        // Contract
        if( is_null($agency) && is_null($city)){
            return $this->redirect($this->generateUrl('bj_offer_listing.contract',array(
                'contract'=>$contract
            )));
        }
        // Contract - Agency
        else if( !is_null($agency) && is_null($city)){
            return $this->redirect($this->generateUrl('bj_offer_listing.contract.agency',array(
                'contract'=>$contract,
                'agency'=>$agency
            )));
        }
        //Contract - City
        else if( is_null($agency) && !is_null($city)){
            return $this->redirect($this->generateUrl('bj_offer_listing.contract.city',array(
                'contract'=>$contract,
                'city'=>$city
            )));
        }
        // Contract - Agency - City
        else if( !is_null($agency) && !is_null($city)){
            return $this->redirect($this->generateUrl('bj_offer_listing.contract.agency.city',array(
                'contract'=>$contract,
                'agency'=>$agency,
                'city'=>$city
            )));
        }

        return new Response('Error Form');
    }

    /**
     * Contract Filter
     * @param $contract
     * @return Response
     */
    public function listingContractFilterAction($contract){

        $options = [
            'path'=>[
                'previous'=>$this->generateUrl('bj_offer_listing.contract.paginate',array(
                    'contract'=>$contract,
                    'iteration'=>0
                )),
                'next'=>$this->generateUrl('bj_offer_listing.contract.paginate',array(
                    'contract'=>$contract,
                    'iteration'=>1
                ))
            ],
            'newsletter'=>[
                'title'=> $contract,
                'target'=> json_encode([
                    'contract'=>$contract,
                    'agency'=>null,
                    'city'=>null
                ]),
                'path'=> $this->generateUrl('bj_offer_newsletter')
            ]
        ];
        return $this->forward('BJFrontBundle:Filter:listingAlgorithm',array(
            'contract'=>$contract,
            'iteration'=>0,
            'agency' => null,
            'city' => null,
            'options'=>$options
        ));
    }

    /**
     * Contract Filter Paginate
     * @param $contract
     * @param $iteration
     * @return Response
     */
    public function listingContractFilterPaginationAction($contract, $iteration){

        $options = [
            'path'=>[
                'previous'=>$this->generateUrl('bj_offer_listing.contract.paginate',array(
                    'contract'=>$contract,
                    'iteration'=>$iteration-1
                )),
                'next'=>$this->generateUrl('bj_offer_listing.contract.paginate',array(
                    'contract'=>$contract,
                    'iteration'=>$iteration+1
                ))
            ],
            'newsletter'=>[
                'title'=> $contract,
                'target'=> json_encode([
                    'contract'=>$contract,
                    'agency'=>null,
                    'city'=>null
                ]),
                'path'=> $this->generateUrl('bj_offer_newsletter')
            ]
        ];
        return $this->forward('BJFrontBundle:Filter:listingAlgorithm',array(
            'contract'=>$contract,
            'iteration'=>$iteration,
            'agency' => null,
            'city' => null,
            'options'=>$options
        ));
    }

    /**
     * Contract Agency Filter
     * @param $contract
     * @param $agency
     * @return Response
     */
    public function listingContractAgencyFilterAction($contract, $agency){

        $agency_r = str_replace('-',' ',$agency);
        $options = [
            'path'=>[
                'previous'=>$this->generateUrl('bj_offer_listing.contract.agency.paginate',array(
                    'contract'=>$contract,
                    'agency'=>$agency,
                    'iteration'=>0
                )),
                'next'=>$this->generateUrl('bj_offer_listing.contract.agency.paginate',array(
                    'contract'=>$contract,
                    'agency'=>$agency,
                    'iteration'=>1
                ))
            ],
            'newsletter'=>[
                'title'=> $contract . ' from ' . $agency_r,
                'target'=> json_encode([
                    'contract'=>$contract,
                    'agency'=>$agency_r,
                    'city'=>null
                ]),
                'path'=> $this->generateUrl('bj_offer_newsletter')
            ]
        ];
        return $this->forward('BJFrontBundle:Filter:listingAlgorithm',array(
            'contract'=>$contract,
            'iteration'=>0,
            'agency' => $agency_r,
            'city' => null,
            'options'=>$options
        ));
    }

    /**
     * Contract Agency Filter Paginate
     * @param $contract
     * @param $agency
     * @param $iteration
     * @return Response
     */
    public function listingContractAgencyFilterPaginationAction($contract, $agency, $iteration){

        $agency_r = str_replace('-',' ',$agency);
        $options = [
            'path'=>[
                'previous'=>$this->generateUrl('bj_offer_listing.contract.agency.paginate',array(
                    'contract'=>$contract,
                    'agency'=>$agency,
                    'iteration'=>$iteration-1
                )),
                'next'=>$this->generateUrl('bj_offer_listing.contract.agency.paginate',array(
                    'contract'=>$contract,
                    'agency'=>$agency,
                    'iteration'=>$iteration+1
                ))
            ],
            'newsletter'=>[
                'title'=> $contract . ' from ' . $agency_r,
                'target'=> json_encode([
                    'contract'=>$contract,
                    'agency'=>$agency_r,
                    'city'=>null
                ]),
                'path'=> $this->generateUrl('bj_offer_newsletter')
            ]
        ];
        return $this->forward('BJFrontBundle:Filter:listingAlgorithm',array(
            'contract'=>$contract,
            'iteration'=>$iteration,
            'agency' => $agency_r,
            'city' => null,
            'options'=>$options
        ));
    }

    /**
     * Contract City Filter
     * @param $contract
     * @param $city
     * @return Response
     */
    public function listingContractCityFilterAction($contract, $city){

        $options = [
            'path'=>[
                'previous'=>$this->generateUrl('bj_offer_listing.contract.city.paginate',array(
                    'contract'=>$contract,
                    'city'=>$city,
                    'iteration'=>0
                )),
                'next'=>$this->generateUrl('bj_offer_listing.contract.city.paginate',array(
                    'contract'=>$contract,
                    'city'=>$city,
                    'iteration'=>1
                ))
            ],
            'newsletter'=>[
                'title'=> $contract . ' in ' . $city,
                'target'=> json_encode([
                    'contract'=>$contract,
                    'agency'=>null,
                    'city'=>$city
                ]),
                'path'=> $this->generateUrl('bj_offer_newsletter')
            ]
        ];
        return $this->forward('BJFrontBundle:Filter:listingAlgorithm',array(
            'contract'=>$contract,
            'iteration'=>0,
            'agency' => null,
            'city' => $city,
            'options'=>$options
        ));
    }

    /**
     * Contract City Filter Paginate
     * @param $contract
     * @param $city
     * @param $iteration
     * @return Response
     */
    public function listingContractCityFilterPaginationAction($contract, $city, $iteration){

        $options = [
            'path'=>[
                'previous'=>$this->generateUrl('bj_offer_listing.contract.city.paginate',array(
                    'contract'=>$contract,
                    'city'=>$city,
                    'iteration'=>$iteration-1
                )),
                'next'=>$this->generateUrl('bj_offer_listing.contract.city.paginate',array(
                    'contract'=>$contract,
                    'city'=>$city,
                    'iteration'=>$iteration+1
                ))
            ],
            'newsletter'=>[
                'title'=> $contract . ' in ' . $city,
                'target'=> json_encode([
                    'contract'=>$contract,
                    'agency'=>null,
                    'city'=>$city
                ]),
                'path'=> $this->generateUrl('bj_offer_newsletter')
            ]
        ];
        return $this->forward('BJFrontBundle:Filter:listingAlgorithm',array(
            'contract'=>$contract,
            'iteration'=>$iteration,
            'agency' => null,
            'city' => $city,
            'options'=>$options
        ));
    }

    /**
     * Contract Agency City Filter
     * @param $contract
     * @param $agency
     * @param $city
     * @return Response
     */
    public function listingContractAgencyCityFilterAction($contract, $agency, $city){

        $agency_r = str_replace('-',' ',$agency);
        $options = [
            'path'=>[
                'previous'=>$this->generateUrl('bj_offer_listing.contract.agency.city.paginate',array(
                    'contract'=>$contract,
                    'agency'=>$agency,
                    'city'=>$city,
                    'iteration'=>0
                )),
                'next'=>$this->generateUrl('bj_offer_listing.contract.agency.city.paginate',array(
                    'contract'=>$contract,
                    'agency'=>$agency,
                    'city'=>$city,
                    'iteration'=>1
                ))
            ],
            'newsletter'=>[
                'title'=> $contract . ' from '. $agency_r . ' in ' . $city,
                'target'=> json_encode([
                    'contract'=>$contract,
                    'agency'=>$agency_r,
                    'city'=>$city
                ]),
                'path'=> $this->generateUrl('bj_offer_newsletter')
            ]
        ];
        return $this->forward('BJFrontBundle:Filter:listingAlgorithm',array(
            'contract'=>$contract,
            'iteration'=>0,
            'agency' => $agency_r,
            'city' => $city,
            'options'=>$options
        ));
    }

    /**
     * Contract Agency City Filter Paginate
     * @param $contract
     * @param $agency
     * @param $city
     * @param $iteration
     * @return Response
     */
    public function listingContractAgencyCityFilterPaginationAction($contract, $agency, $city, $iteration){

        $agency_r = str_replace('-',' ',$agency);
        $options = [
            'path'=>[
                'previous'=>$this->generateUrl('bj_offer_listing.contract.agency.city.paginate',array(
                    'contract'=>$contract,
                    'agency'=>$agency,
                    'city'=>$city,
                    'iteration'=>$iteration-1
                )),
                'next'=>$this->generateUrl('bj_offer_listing.contract.agency.city.paginate',array(
                    'contract'=>$contract,
                    'agency'=>$agency,
                    'city'=>$city,
                    'iteration'=>$iteration+1
                ))
            ],
            'newsletter'=>[
                'title'=> $contract . ' from '. $agency_r . ' in ' . $city,
                'target'=> json_encode([
                    'contract'=>$contract,
                    'agency'=>$agency_r,
                    'city'=>$city
                ]),
                'path'=> $this->generateUrl('bj_offer_newsletter')
            ]
        ];
        return $this->forward('BJFrontBundle:Filter:listingAlgorithm',array(
            'contract'=>$contract,
            'iteration'=>$iteration,
            'agency' => $agency_r,
            'city' => $city,
            'options'=>$options
        ));
    }

    /**
     * Rendering Algorithm
     * @param $contract
     * @param $agency
     * @param $city
     * @param $iteration
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function listingAlgorithmAction($contract, $agency, $city, $iteration, $options){
        $max = 10;
        $offers=[];
        $count=0;

        // Contract
        if( is_null($agency) && is_null($city)){

            $offers = $this->getDoctrine()
                ->getRepository('BJCorporateBundle:Offer')
                ->findOffersByContract( $contract, $iteration, $max)
            ;

            $count = $this->getDoctrine()
                ->getRepository('BJCorporateBundle:Offer')
                ->findCountOffersByContract( $contract )
            ;
        }
        // Contract - Agency
        else if( !is_null($agency) && is_null($city)){

            $offers = $this->getDoctrine()
                ->getRepository('BJCorporateBundle:Offer')
                ->findOffersByContractAgency( $contract, $agency, $iteration, $max)
            ;

            $count = $this->getDoctrine()
                ->getRepository('BJCorporateBundle:Offer')
                ->findCountOffersByContractAgency( $contract, $agency )
            ;

        }
        // Contract City
        else if( is_null($agency) && !is_null($city)){

            $offers = $this->getDoctrine()
                ->getRepository('BJCorporateBundle:Offer')
                ->findOffersByContractCity( $contract, $city, $iteration, $max)
            ;

            $count = $this->getDoctrine()
                ->getRepository('BJCorporateBundle:Offer')
                ->findCountOffersByContractCity( $contract, $city )
            ;

        }
        // Contract - Agency - City
        else if( !is_null($agency) && !is_null($city)){

            $offers = $this->getDoctrine()
                ->getRepository('BJCorporateBundle:Offer')
                ->findOffersByContractAgencyCity( $contract, $agency, $city, $iteration, $max)
            ;

            $count = $this->getDoctrine()
                ->getRepository('BJCorporateBundle:Offer')
                ->findCountOffersByContractAgencyCity( $contract, $agency, $city )
            ;

        }

        $offerCountIterationPlus =
            $count - (($iteration+1)*$max)
        ;

        if(count($offers)==0 && $iteration!=0){
            return $this->redirect($options['path']['previous']);
        }


        $engine = $this->get('search_engine');
        $option = $engine->filterOptions();
        $settings=[
            'offers'=>$offers,
            'count'=>$count,
            'param'=> [
                'contract'=> $contract,
                'tags'=>[],
                'iterationPlus'=>$offerCountIterationPlus
            ],
            'paginate'=>$iteration,
            'newsletter'=>$options['newsletter'],
            'path'=>$options['path'],
            'filter'=>true,
            'option'=>$option,
            'selected'=>[
                'contract'=>$contract,
                'agency'=>$agency,
                'city'=>$city
            ]
        ];

        return $this->render('BJFrontBundle:Default:job_listing.html.twig',array(
            'settings'=>$settings
        ));
    }

}