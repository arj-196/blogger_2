<?php

namespace BJ\FrontBundle\Controller;

use BJ\CorporateBundle\Entity\Enterprise;
use BJ\CorporateBundle\Entity\Offer;
use BJ\ElementBundle\Entity\Address;
use BJ\ElementBundle\Entity\Email;
use BJ\ElementBundle\Entity\Phone;
use BJ\ElementBundle\Entity\Position;
use BJ\CorporateBundle\Entity\Tracker;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostController extends Controller
{
    /**
     * DEPRECIATED
     * Select If register or post quick
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        //Verify if user logged in
        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('bj_post_quick_offer'));
        }

        $settings = [
            'action' => 'index'
        ];
        return $this->render('BJFrontBundle:Post:index.html.twig', array(
            'settings' => $settings
        ));
    }

    /**
     * Offer Form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function offerAction()
    {
        // DEPRECIATED
        $securityContext = $this->container->get('security.context');
        $data = [];
        $user = null;
        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $securityContext->getToken()->getUser();
            $enterprises = $user->getEnterprises();
            $data['user'] = [
                'email' => $user->getEmail()
            ];
            if (count($enterprises) > 0) {
                $offers = $enterprises[0]->getOffers();
                if (count($offers) > 0) {
                    $data['address'] = $offers[0]->getAddress();
                    $data['email'] = $offers[0]->getEmail();
                    $data['phone'] = $offers[0]->getPhone();
                }
            }
        }
        // END DEPRECIATED


        $settings = [
            'action' => 'offer', //dynamic loading in index.html.twig
            'userData' => $data,
            'url' => $this->generateUrl('bj_post_quick_offer_posted'),
            'user'=> $user,
        ];

        return $this->render('BJFrontBundle:Post:index.html.twig', array(
            'settings' => $settings
        ));
    }

    /**
     * Upload Offer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postOfferAction(Request $request)
    {
        $data = $request->request;

        return $this->forward('BJFrontBundle:Post:handleFormData',array(
            'data'=>$data,
            'enterprise' => new Enterprise(),
            'offer' => new Offer(),
            'options'=>null,
            'mode' => 'NEW_OFFER'
        ));
    }

    public function handleFormDataAction($data, $enterprise, $offer, $mode, $options){
        $engine = $this->get('offer_form');
        $settings = $engine->handle($mode, $data);

        if($mode == 'NEW_OFFER'){

            return $this->render('BJFrontBundle:Post:status.html.twig', array(
                'settings' => $settings
            ));
        }

        return new Response('');
    }

    /**
     * DEPRECIATED ----
     * Handing the form data
     * For NEW_OFFER, ENTERPRISE_EDIT, SUPER_EDIT
     * @param $data
     * @param $enterprise
     * @param $offer
     * @param $mode
     * @return Response
     */
    public function handleFormDataAction2($data, $enterprise, $offer, $mode, $options)
    {

        // Default actions
        if(true){
            $enterprise->setName(trim($data->get('f-c-n')));
            $enterprise->setType(trim($data->get('f-c-n-1')));
            $enterprise->setDescription("");
            $address = new Address(trim($data->get('f-l-1')), trim($data->get('f-l-2')), trim($data->get('f-l-3')), trim($data->get('f-l-4')));
            $add = $address->getStreet() . ', ' . $address->getCity()
                . ', ' . $address->getCountry() . ', ' . $address->getPostal();

            //Getting GPS Location
            $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($add) . '&sensor=true';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response);
            $pos = new Position();

            if (isset($response_a) && isset($response_a->results[0])
                && isset($response_a->results[0]->geometry)
                && isset($response_a->results[0]->geometry->location)
            ) {
                $pos->setLatitude($response_a->results[0]->geometry->location->lat);
                $pos->setLongitude($response_a->results[0]->geometry->location->lng);
                $offer->setPosition($pos);
            }

            $offer->setEmail(new Email(trim($data->get('f-e'))));
            $offer->setPhone(new Phone(trim($data->get('f-p'))));
            $offer->setAddress($address);
            $offer->setTitle(trim($data->get('f-o-t')));
            $offer->setDescription(trim($data->get('f-o-d')));
            $offer->setMission(trim($data->get('f-o-d-m')));
            $offer->setProfile(trim($data->get('f-o-d-p')));
            $offer->setTags(trim($data->get('tags')));
            $offer->setPrivacy(intval($data->get('privacy')));
            $post = $data->get('f-pt-2');
            if (empty($post)) {
                $post = $data->get('f-pt-1');
                if (empty($post)) {
                    $offer->setPost('');
                } else {
                    $offer->setPost(trim($data->get('f-pt-1')));
                }
            } else {
                $offer->setPost(trim($post));
            }

            $offer->setType(trim($data->get('f-c-t')));

            $salary = $data->get('f-c-s-1');
            $offer->setSalary(trim($salary));
//        preg_match_all('!\d+!', $salary, $matches);
//        if(count($matches)>0 && count($matches[0])>0){
//            $offer->setSalary($matches[0][0]);
//        }else{
//            $offer->setSalary($salary);
//        }

            $asap = $data->get('f-d-a');
            if (isset($asap)) {
                $offer->setAsap(true);
            } else {
                $offer->setAsap(false);
            }

            $duree = $data->get('f-du-2');
            if (empty($duree)) {
                $offer->setDuration($data->get('f-du'));
            } else {
                preg_match_all('!\d+!', $duree, $matches);
                if (count($matches) > 0 && count($matches[0]) > 0) {
                    $offer->setDuration($matches[0][0]);
                } else {
                    $offer->setDuration($duree);
                }
            }
            $offer->setDateStart(new \DateTime($data->get('f-d-s')));
        }

        //If uploading a new offer
        if($mode == 'NEW_OFFER')
        {
            $tracker = new Tracker();
            $tracker->setAuthorised(false);
            $tracker->setViewed(false);
            $tracker->setOffer($offer);

            $offer->setTracker($tracker);
            $offer->setEnterprise($enterprise);
            $enterprise->addOffer($offer);


            //Saving to database
            $em = $this->getDoctrine()->getManager();
            $em->persist($offer);
            $em->persist($enterprise);
            $em->persist($tracker);
            $em->flush();
            $em->refresh($enterprise);

            //Verify if user logged in
            $securityContext = $this->container->get('security.context');
            if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
                $user = $securityContext->getToken()->getUser();
                if(!$user->existEnterprise($enterprise)){
                    $user->addEnterprise($enterprise);
                    $userManager = $this->get('fos_user.user_manager');
                    $userManager->updateUser($user);
                }
            }

            //Send mail to user
            $mailer = $this->get('mailer');
            $message = $mailer->createMessage()
                ->setSubject('LLLITL : Uploaded Offer')
                ->setFrom('arjruler93@gmail.com')
                ->setTo(
                    $offer->getEmail()->getEmail()
                )
                ->setBody(
                    $this->renderView(
                        ':Emails:job_upload.html.twig', array(
                            'offer' => $offer
                        )
                    ),
                    'text/html'
                );
            $mailer->send($message);

            $settings = [
                'action' => 'posted',
                'enterprise' => $enterprise,
                'offer' => $offer
            ];

            //TODO if enterprise exist

            return $this->render('BJFrontBundle:Post:index.html.twig', array(
                'settings' => $settings
            ));
        }

        //If enterprise edit offer
        elseif($mode=='ENTERPRISE_EDIT'){

            $user = $options['user'];
            $request = $options['request'];
            $count = $options['count'];

            //Saving to database
            $em = $this->getDoctrine()->getManager();
            $em->persist($offer);
            $em->flush();

            if(in_array('ROLE_SUPER_ADMIN',$user->getRoles())){
                //If super user
                $referer_url = $request->headers->get('referer');
                return $this->redirect($referer_url);

            }else{
                return $this->redirect($this->generateUrl('user.enterprise.dashboard.view.enterprise', array('count' => $count)));
            }

        }

        // If super edit offer
        elseif($mode=='SUPER_EDIT'){

            $id = $options['id'];

            //Saving to database
            $em = $this->getDoctrine()->getManager();
            $em->persist($offer);
            $em->flush();

            return $this->redirect($this->generateUrl('user.admin.super.dashboard.edit.offer', array('id' => $id)));
        }


        return new Response('');
    }

    /**
     * ENTERPRISE CONTROL
     * Editing an Offer
     * @param $count
     * @param $offerCount
     * @return Response
     */
    public function editAction($count, $offerCount)
    {
        $user = $this->getUser();
        $enterprise = $user->getEnterprises()[$count];
        $offer = $enterprise->getOffers()[$offerCount];

        $data['user'] = [
            'email' => $user->getEmail()
        ];
        $data['enterprise'] = $enterprise;

        $data['address'] = $offer->getAddress();
        $data['email'] = $offer->getEmail();
        $data['phone'] = $offer->getPhone();
        $data['offer'] = $offer;

        $settings = [
            'action' => 'offer', //dynamic loading in index.html.twig
            'userData' => $data,
            'url' => $this->generateUrl('user.enterprise.dashboard.edit.offer.post.action', array('count' => $count, 'offerCount' => $offerCount))
        ];

        return $this->render('BJFrontBundle:Post:index.html.twig', array(
            'settings' => $settings
        ));
    }


    /**
     * ENTERPRISE CONTROL
     * Updating Offer details after edit
     * @param Request $request
     * @param $count
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function postEditAction(Request $request, $count, $offerCount)
    {
        $data = $request->request;
        $user = $this->getUser();
        $enterprise = $user->getEnterprises()[$count];
        $offer = $enterprise->getOffers()[$offerCount];


        return $this->forward('BJFrontBundle:Post:handleFormData',array(
            'data'=>$data,
            'enterprise' => $enterprise,
            'offer' => $offer,
            'options'=>[ 'user'=>$user, 'request'=>$request, 'count'=>$count ],
            'mode' => 'ENTERPRISE_EDIT'
        ));
    }

    /**
     * SUPER ADMIN CONTROL
     * Updating offer details
     * @param $id
     * @return Response
     */
    public function adminEditAction($id)
    {
        $offer = $this->getDoctrine()->getRepository('BJCorporateBundle:Offer')
            ->find($id);
        $data['address'] = $offer->getAddress();
        $data['email'] = $offer->getEmail();
        $data['phone'] = $offer->getPhone();
        $data['offer'] = $offer;
        $enterprise = $offer->getEnterprise();
        $data['enterprise'] = $enterprise;

        try{
            //accessing database directly
            $host = $this->container->getParameter('database_host');
            $dbname = $this->container->getParameter('database_name');
            $user = $this->container->getParameter('database_user');
            $pass = $this->container->getParameter('database_password');

            $db = new \PDO('mysql:host='.$host.';dbname='.$dbname.';charset=utf8',
                $user, $pass);

            //fetching username
            $query = ' select username'.
                ' from fos_user'.
                ' where id ='.
                '   (select user_id from user_enterprises'.
                ' where enterprise_id = '.$enterprise->getId().')'
            ;

            $sth = $db->prepare($query);
            $sth->execute();
            $user_id = $sth->fetchAll(\PDO::FETCH_ASSOC);

            if(!empty($user_id)){
                $username = $user_id[0]['username'];
                $userManager = $this->get('fos_user.user_manager');
                $user = $userManager->findUserByUsername($username);

                //locating enterprise index
                $index_enterprise=-1;
                $enterprises = $user->getEnterprises();
                foreach($enterprises as $k=>$v){
                    if($enterprise->getId()==$v->getId()){
                        $index_enterprise=$k;
                        break;
                    }
                }
                //locating offer index
                $index_offer=-1;

                foreach($enterprises[$index_enterprise]->getOffers() as $k=>$v){
                    if($offer->getId()==$v->getId()){
                        $index_offer=$k;
                        break;
                    }
                }

                if($index_offer!=-1 && $index_enterprise!=-1){
                    $data['user'] = [
                        'email' => $user->getEmail()
                    ];

                    $settings = [
                        'action'=>'offer', //dynamic loading in index.html.twig
                        'userData'=>$data,
                        'url'=>$this->generateUrl('user.admin.super.dashboard.edit.offer.post',array('id'=>$offer->getId()))
                    ];
                    return $this->render('BJFrontBundle:Post:index.html.twig',array(
                        'settings'=>$settings
                    ));

                }else{
                    //TODO throw exception
                }
            }
        }
        catch (\PDOException $e){
            //TODO throw exception
        }

        return new Response('');

    }

    /**
     * SUPER ADMIN CONTROL
     * Updating Offer details after edit
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @internal param $count
     */
    public function adminPostEditAction(Request $request, $id)
    {
        $data = $request->request;
        $offer = $this->getDoctrine()->getRepository('BJCorporateBundle:Offer')
            ->find($id);


        return $this->forward('BJFrontBundle:Post:handleFormData',array(
            'data'=>$data,
            'enterprise' => new Enterprise(),
            'offer' => $offer,
            'options'=>['id'=>$id ],
            'mode' => 'SUPER_EDIT'
        ));
    }


}
