<?php
namespace BJ\SecurityBundle\Component\Registration\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Listener responsible to change the redirection after confirm of registration
 * Class RegistrationConfirmationListener
 * @package VM\UserBundle\EventListener
 */
class RegistrationConfirmationListener implements EventSubscriberInterface{

    private $router;

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
        );
    }

    public function onRegistrationSuccess(FormEvent $event)
    {
        $user = $event->getForm()->getData();
        $type = $user->getType();

        if($type == 'client'){
            $roles = array('ROLE_CLIENT');
            $user->setRoles($roles);

            $url = $this->router->generate('user.client.dashboard');
            $event->setResponse(new RedirectResponse($url));

        }else if($type == 'enterprise'){
            $roles = array('ROLE_ENTERPRISE');
            $user->setRoles($roles);

            $url = $this->router->generate('user.enterprise.dashboard');
            $event->setResponse(new RedirectResponse($url));

        }else{
            //
            $roles = array('ROLE_ENTERPRISE');
            $user->setRoles($roles);

            $url = $this->router->generate('user.enterprise.dashboard');
            $event->setResponse(new RedirectResponse($url));

//            $user->setEnabled(false);
//            $url = $this->router->generate('bj_front_homepage');
//            $event->setResponse(new RedirectResponse($url));
        }


    }

}