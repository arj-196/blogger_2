<?php

namespace BJ\SecurityBundle\Component\Authentication\Handler;

use AppBundle\Component\ErrorLog;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{

    protected $router;
    protected $security;
    private $log;
    private $manager;

    public function __construct(Router $router, SecurityContext $security, ErrorLog $log, TokenStorage $manager)
    {
        $this->router = $router;
        $this->security = $security;
        $this->log = $log;
        $this->manager = $manager;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $response = null;

        if ($this->security->isGranted('ROLE_SUPER_ADMIN'))
        {

            $response = new RedirectResponse($this->router->generate('user.admin.super.dashboard'));
        }
        elseif ($this->security->isGranted('ROLE_ADMIN'))
        {
            $response = new RedirectResponse($this->router->generate('bj_front_homepage'));

        }

        elseif ($this->security->isGranted('ROLE_ENTERPRISE'))
        {
//            $response = new RedirectResponse($this->router->generate('user.enterprise.dashboard'));
            $response = new RedirectResponse($this->router->generate('fos_user_security_logout'));

        }

        elseif ($this->security->isGranted('ROLE_CLIENT'))
        {
//            $response = new RedirectResponse($this->router->generate('user.client.dashboard'));
            $response = new RedirectResponse($this->router->generate('fos_user_security_logout'));
        }


        elseif ($this->security->isGranted('ROLE_USER'))
        {

            $response = new RedirectResponse($this->router->generate('bj_front_homepage'));

            // redirect the user to where they were before the login process begun.
//            $referer_url = $request->headers->get('referer');
//            $response = new RedirectResponse($referer_url);
        }

        return $response;
    }

}
