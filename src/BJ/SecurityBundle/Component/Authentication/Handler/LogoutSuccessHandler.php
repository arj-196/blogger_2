<?php

namespace BJ\SecurityBundle\Component\Authentication\Handler;

use AppBundle\Component\ErrorLog;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{

    protected $router;
    private $log;

    public function __construct(Router $router, ErrorLog $log)
    {
        $this->router = $router;
        $this->log = $log;
    }

    public function onLogoutSuccess(Request $request)
    {


        // redirect the user to where they were before the login process begun.
//        $referer_url = $request->headers->get('referer');
//        $response = null;

//        if(!is_null($referer_url))
//            $response = new RedirectResponse($referer_url);
//        else
            $response = new RedirectResponse($this->router->generate('bj_front_homepage'));


        return $response;
    }

}