<?php

namespace BJ\CorporateBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OfferType extends  AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title','text')
            ->add('description','textarea',array('attr'=>array(
                'required'  => false,
                'rows'=>10
            )))
            ->add('mission','textarea',array('attr'=>array(
                'required'  => false,
                'rows'=>10
            )))
            ->add('profile','textarea',array('attr'=>array(
                'required'  => false,
                'rows'=>10
            )))
            ->add('asap', 'checkbox', array(
                'required'  => false,
            ))
            ->add('dateStart','date')
            ->add('duration','text')

//            ->add('duration','choice',array(
//                'choices'=>array(
//                    '3'=>'3 months',
//                    '4'=>'4 months',
//                    '6'=>'6 months',
//                    '6+'=>'6 months plus',
//                ),
//                'required'  => false,
//                'expanded'=>true,
//                'multiple'=>false
//            ))

            ->add('type','choice',array(
                'required'  => false,
                'choices'=>array(
                    'stage'=>'stage',
                    'cdi'=>'cdi',
                    'cdd'=>'cdd',
                    'alternance'=>'alternance',
                    'freelance'=>'freelance'
                )
            ))

            ->add('post','text',array('attr'=>array(
                'required'  => true,
            )))

            ->add('salary','text',array('attr'=>array(
                'required'  => true,
            )))

            ->add('privacy','choice',array(
                'required'  => false,
                'choices'=>array(
                    '0'=>'Public',
                    '1'=>'Private'
                ),
                'expanded'=>true,
                'multiple'=>false
            ))

//            ->add('tags','hidden',array(
//                'data'=>'[]'
//            ))
            ->add('update','submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BJ\CorporateBundle\Entity\Offer'
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'bj_corporate_offer';
    }


}