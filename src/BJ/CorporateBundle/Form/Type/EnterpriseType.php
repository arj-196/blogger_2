<?php

namespace BJ\CorporateBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EnterpriseType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // add your custom field
        $builder
            ->add('name','text')
            ->add('type','choice',array(
                'choices'=>array(
                    'agence'=>'agence',
                    'announceur'=>'announceur'
                )
            ))
            ->add('description','textarea')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BJ\CorporateBundle\Entity\Enterprise'
        ));
    }

    public function getName()
    {
        return 'bj_corporate_enterprise';
    }

}