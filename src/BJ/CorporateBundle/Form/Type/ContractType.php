<?php

namespace BJ\CorporateBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class ContractType extends  AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title','choice',array(
                'choices'=>array(
                    'stage'=>'stage',
                    'cdi'=>'cdi',
                    'cdd'=>'cdd'
                )
            ))
            ->add('salStart','number')
            ->add('salEnd','number')
            ->add('update','submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BJ\CorporateBundle\Entity\Contract'
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'vm_corporate_contract';
    }


}