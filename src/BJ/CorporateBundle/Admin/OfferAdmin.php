<?php

namespace BJ\CorporateBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class OfferAdmin extends Admin{

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', 'text', array('label' => 'Offer Title'))
            ->add('tags', 'text', array('label' => 'Offer Tags'))
            ->add('description', 'text', array('label' => 'Offer Description'))
            ->add('contract', 'entity', array('class' => 'BJ\CorporateBundle\Entity\Contract'))
            ->add('validity', 'entity', array('class' => 'BJ\ElementBundle\Entity\Validity'))
            ->add('enterprise', 'entity', array('class' => 'BJ\CorporateBundle\Entity\Enterprise'))
            ->add('tracker', 'entity', array('class' => 'BJ\CorporateBundle\Entity\Tracker') )

        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('tags')
            ->add('enterprise')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title')
            ->add('tags')
            ->add('contract')
            ->add('validity')
            ->add('enterprise')
            ->add('tracker')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
}