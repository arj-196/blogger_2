<?php

namespace BJ\CorporateBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class TrackerAdmin extends Admin{
    protected $parentAssociationMapping = 'offer';

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('viewed', 'checkbox', array(
                'label' => 'If Viewed',
                'required'  => false
            ))
            ->add('authorised', 'checkbox', array(
                'label' => 'If Authorised',
                'required'  => false
                ))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('viewed')
            ->add('authorised')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('viewed')
            ->add('authorised')

        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('list');
        $collection->remove('create');
    }
}
