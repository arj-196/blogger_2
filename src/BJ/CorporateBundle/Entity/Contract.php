<?php

namespace BJ\CorporateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\SerializerBuilder;

/**
 * Contract
 *
 * @ORM\Table(name="contract")
 * @ORM\Entity
 */
class Contract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var float
     *
     * @ORM\Column(name="salStart", type="float", nullable=true)
     */
    private $salStart;

    /**
     * @var float
     *
     * @ORM\Column(name="salEnd", type="float", nullable=true)
     */
    private $salEnd;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Contract
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set salStart
     *
     * @param float $salStart
     * @return Contract
     */
    public function setSalStart($salStart)
    {
        $this->salStart = $salStart;

        return $this;
    }

    /**
     * Get salStart
     *
     * @return float 
     */
    public function getSalStart()
    {
        return $this->salStart;
    }

    /**
     * Set salEnd
     *
     * @param float $salEnd
     * @return Contract
     */
    public function setSalEnd($salEnd)
    {
        $this->salEnd = $salEnd;

        return $this;
    }

    /**
     * Get salEnd
     *
     * @return float 
     */
    public function getSalEnd()
    {
        return $this->salEnd;
    }

    /**
     * @return string
     */
    public function __toString() {
        return
            $this->title
//            . ' for: ' .
//            $this->salStart
//            . ' - ' .
//            $this->salEnd
        ;
//        $serializer = SerializerBuilder::create()
//            ->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy())
//            ->build();
//
//        return $serializer->serialize($this, 'json');
    }
}
