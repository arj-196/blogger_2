<?php

namespace BJ\CorporateBundle\Entity;

use BJ\ElementBundle\Entity\Address;
use BJ\ElementBundle\Entity\Email;
use BJ\ElementBundle\Entity\Phone;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\SerializerBuilder;

/**
 * Offer
 *
 * @ORM\Table(name="offer")
 * @ORM\Entity(repositoryClass="BJ\CorporateBundle\Entity\OfferRepository")
 */
class Offer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="post", type="string", length=255, nullable=true)
     */
    private $post;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="mission", type="text", nullable=true)
     */
    private $mission;

    /**
     * @var string
     *
     * @ORM\Column(name="profile", type="text", nullable=true)
     */
    private $profile;

    /**
     * @var integer
     *
     * @ORM\Column(name="asap", type="boolean", nullable=true)
     */
    private $asap;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStart", type="date", nullable=true)
     */
    private $dateStart;

    /**
     * @var string
     *
     * @ORM\Column(name="duration", type="string", length=255, nullable=true)
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="salary", type="text", nullable=true)
     */
    private $salary;

    /**
     * @ORM\ManyToOne(targetEntity="Enterprise", inversedBy="offers")
     * @ORM\JoinColumn(name="enterprise_id", referencedColumnName="id")
     **/
    protected $enterprise;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="date")
     */
    private $createdAt;

    /**
     * @ORM\OneToOne(targetEntity="BJ\CorporateBundle\Entity\Tracker", mappedBy="offer", cascade={"persist", "remove"})
     **/
    private $tracker;

    /**
     * @var integer
     *
     * @ORM\Column(name="privacy", type="integer", nullable=true)
     */
    private $privacy;

    /**
     * Depreciated
     * @var string
     *
     * @ORM\Column(name="tags", type="text", nullable=true)
     */
    private $tags;

    /**
     * @ORM\OneToOne(targetEntity ="BJ\ElementBundle\Entity\Position",  cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="position_id", referencedColumnName="id", nullable=true, unique=false)
     */
    protected $position;

    /**
     * @ORM\OneToOne(targetEntity ="BJ\ElementBundle\Entity\Address",  cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id", nullable=true, unique=false)
     */
    protected $address;

    /**
     * @ORM\OneToOne(targetEntity ="BJ\ElementBundle\Entity\Email",  cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="email_id", referencedColumnName="id", nullable=true, unique=false)
     */
    protected $email;

    /**
     * @ORM\OneToOne(targetEntity ="BJ\ElementBundle\Entity\Phone",  cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="phone_id", referencedColumnName="id", nullable=true, unique=false)
     */
    protected $phone;

    /**
     * Constructor
     */
    public function __construct(){
        $this->createdAt = new \DateTime('now');
        $this->address = new Address();
        $this->email = new Email();
        $this->phone = new Phone();

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Offer
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Offer
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set enterprise
     *
     * @param \BJ\CorporateBundle\Entity\Enterprise $enterprise
     * @return Offer
     */
    public function setEnterprise(\BJ\CorporateBundle\Entity\Enterprise $enterprise = null)
    {
        $this->enterprise = $enterprise;

        return $this;
    }

    /**
     * Get enterprise
     *
     * @return \BJ\CorporateBundle\Entity\Enterprise 
     */
    public function getEnterprise()
    {
        return $this->enterprise;
    }

    /**
     * Get tags
     *
     * @return string
     */
    public function getTags()
    {
       return $this->tags;
    }

    /**
     * Set tags
     *
     * @param string $tags
     * @return Offer
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * Add tag
     *
     * @param string $tag
     * @return Offer
     */
    public function addTag($tag)
    {
        $tags = json_decode($this->tags);
        $tags[] = $tag;
        $this->tags = json_encode($tags);
        return $this;
    }

    /**
     * Remove tag
     *
     * @param string $tag
     * @return Offer
     */
    public function removeTag($tag)
    {
        $tags = json_decode($this->tags);
        $key = array_search('English',$tags);
        if($key){
            unset($tags[$key]);
            $this->tags = json_encode($tags);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->title;
    }


    /**
     * Set tracker
     *
     * @param \BJ\CorporateBundle\Entity\Tracker $tracker
     * @return Offer
     */
    public function setTracker(\BJ\CorporateBundle\Entity\Tracker $tracker = null)
    {
        $this->tracker = $tracker;

        return $this;
    }

    /**
     * Get tracker
     *
     * @return \BJ\CorporateBundle\Entity\Tracker 
     */
    public function getTracker()
    {
        return $this->tracker;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Offer
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set privacy
     *
     * @param integer $privacy
     * @return Offer
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;

        return $this;
    }

    /**
     * Get privacy
     *
     * @return integer 
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * Set mission
     *
     * @param string $mission
     * @return Offer
     */
    public function setMission($mission)
    {
        $this->mission = $mission;

        return $this;
    }

    /**
     * Get mission
     *
     * @return string 
     */
    public function getMission()
    {
        return $this->mission;
    }

    /**
     * Set profile
     *
     * @param string $profile
     * @return Offer
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return string 
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set asap
     *
     * @param integer $asap
     * @return Offer
     */
    public function setAsap($asap)
    {
        $this->asap = $asap;

        return $this;
    }

    /**
     * Get asap
     *
     * @return integer 
     */
    public function getAsap()
    {
        return $this->asap;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return Offer
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set duration
     *
     * @param string $duration
     * @return Offer
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Offer
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set post
     *
     * @param string $post
     * @return Offer
     */
    public function setPost($post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return string 
     */
    public function getPost()
    {
        return $this->post;
    }


    /**
     * Set salary
     *
     * @param float $salary
     * @return Offer
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get salary
     *
     * @return float 
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * Set position
     *
     * @param \BJ\ElementBundle\Entity\Position $position
     * @return Offer
     */
    public function setPosition(\BJ\ElementBundle\Entity\Position $position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return \BJ\ElementBundle\Entity\Position 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set address
     *
     * @param \BJ\ElementBundle\Entity\Address $address
     * @return Offer
     */
    public function setAddress(\BJ\ElementBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \BJ\ElementBundle\Entity\Address 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set email
     *
     * @param \BJ\ElementBundle\Entity\Email $email
     * @return Offer
     */
    public function setEmail(\BJ\ElementBundle\Entity\Email $email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return \BJ\ElementBundle\Entity\Email 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param \BJ\ElementBundle\Entity\Phone $phone
     * @return Offer
     */
    public function setPhone(\BJ\ElementBundle\Entity\Phone $phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return \BJ\ElementBundle\Entity\Phone 
     */
    public function getPhone()
    {
        return $this->phone;
    }
}
