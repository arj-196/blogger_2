<?php

namespace BJ\CorporateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tracker
 *
 * @ORM\Table(name="tracker")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="BJ\CorporateBundle\Entity\TrackerRepository")
 */
class Tracker
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="viewed", type="boolean")
     */
    private $viewed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="authorised", type="boolean")
     */
    private $authorised;

    /**
     * @var integer
     *
     * @ORM\Column(name="expired", type="integer", nullable=true)
     */
    private $expired;

    //TODO encrpt password
    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;


    /**
     * @ORM\OneToOne(targetEntity="BJ\CorporateBundle\Entity\Offer", inversedBy="tracker")
     * @ORM\JoinColumn(name="offer_id", referencedColumnName="id")
     **/
    private $offer;


    public function __construct(){
        $this->authorised = 0;
        $this->viewed = 0;
        $this->expried = 0;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set viewed
     *
     * @param boolean $viewed
     * @return Tracker
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;

        return $this;
    }

    /**
     * Get viewed
     *
     * @return integer 
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * Set authorised
     *
     * @param boolean $authorised
     * @return Tracker
     */
    public function setAuthorised($authorised)
    {
        $this->authorised = $authorised;

        return $this;
    }

    /**
     * Get authorised
     *
     * @return boolean
     */
    public function getAuthorised()
    {
        return $this->authorised;
    }

    public function __toString(){
        $str='';
        if(! $this->viewed){
            $str .= ' NEW ! - ';
        }
        if($this->authorised){
            $str .= ' Visible ';
        }else{
            $str .= ' Hidden';
        }

        return $str;
    }

    /**
     * Set offer
     *
     * @param \BJ\CorporateBundle\Entity\Offer $offer
     * @return Tracker
     */
    public function setOffer(\BJ\CorporateBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Get offer
     *
     * @return \BJ\CorporateBundle\Entity\Offer 
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * Set expired
     *
     * @param integer $expired
     * @return Tracker
     */
    public function setExpired($expired)
    {
        $this->expired = $expired;

        return $this;
    }

    /**
     * Get expired
     *
     * @return integer 
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Tracker
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

}
