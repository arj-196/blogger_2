<?php

namespace BJ\CorporateBundle\Entity;

use BJ\ElementBundle\Entity\Address;
use BJ\ElementBundle\Entity\Email;
use BJ\ElementBundle\Entity\Phone;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\SerializerBuilder;

/**
 * Enterprise
 *
 * @ORM\Table(name="enterprise")
 * @ORM\Entity(repositoryClass="BJ\CorporateBundle\Entity\EnterpriseRepository")
 */
class Enterprise
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text", nullable=true)
     */
    private $url;

    /**
     * @ORM\OneToMany(targetEntity="Offer", mappedBy="enterprise")
     **/
    protected $offers;

    /**
     * Constructor
     */
    public function __construct()
    {

        $this->offers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Enterprise
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Enterprise
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add offers
     *
     * @param \BJ\CorporateBundle\Entity\Offer $offers
     * @return Enterprise
     */
    public function addOffer(\BJ\CorporateBundle\Entity\Offer $offers)
    {
        $this->offers[] = $offers;

        return $this;
    }

    /**
     * Remove offers
     *
     * @param \BJ\CorporateBundle\Entity\Offer $offers
     */
    public function removeOffer(\BJ\CorporateBundle\Entity\Offer $offers)
    {
        $this->offers->removeElement($offers);
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Enterprise
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->name;
//        $serializer = SerializerBuilder::create()
//            ->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy())
//            ->build();
//
//        return $serializer->serialize($this, 'json');
    }



    /**
     * Set type
     *
     * @param string $type
     * @return Enterprise
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
}
